#!/usr/bin/env python3


"""
Makes a 2D Histogram, where the discriminants are on the x-axis, and another variable (pT/eta/etc..)
is on the y-axis. The Z-axis is the number of fat jets satisfying some kinematic selection
and in case of BSM, some truth matching.

This returns a .h5 file which is required by draw_2d_roc and draw_eff_plots scripts

Output is:

1x Group per discriminant being plotted:
    - 1x Group per process
        - 1x Group Attribute -- (min,max) edge for var_bins and discrim_bins ( [xbins,ybins])
        - 1x Group dataset -- 2d histogram of var vs discriminant
        - 1x Group dataset -- 2d histogram with weights= weights^2 of var vs discriminant

"""
from argparse import ArgumentParser
from glob import glob
import os
from pathlib import Path

import numpy as np
from h5py import File

from xbb.common import dhelp
from xbb.common import get_dsid, get_ds_weights_dict, get_pt_eta_reweighting
from xbb.selectors import PROC_SELECTORS

from xbb.var_getters import VARIABLE_GETTERS, VARIABLE_EDGES

from xbb.selectors import (mass_window_higgs, loose_mass_window,
                           fatjet_mass_window_pt_range,
                           fatjet_mass_window_pt_range_truth_match,
                           truth_match_hadron_flav)


# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_VERBOSE_HELP = 'Turn on Verbose run'
_DENOM_HELP = 'The .json file containing DSID:NEventsProcessed Map '
_XS_HELP = 'The .txt file containing space separated info on XS and filter eff'
_PT_HISTS_HELP = dhelp('Path to where the histograms from make_jet_pt_hist.py are stored')
_OUTFILE_HELP = dhelp('Path to (including) file where ROC curves are stored ')
_PT_RANGE_HELP = dhelp('The pT range to plot to apply to fat-jets')
_ROC_DISC_HELP = dhelp('The ROC discriminants to plot curves for')
_VARS_HELP = dhelp('Kinematic Variable to plot against discriminant')
# ================
# Defaults for Args
# =================
DEFAULT_PT_RANGE = (250e3, np.inf)
DEFAULT_DISCRIMS = {'Xbb_V1_ftop025', 'Xbb_V3_ftop025', 'dl1r'}
DISC_CHOICES = set(VARIABLE_GETTERS.keys())
DEFAULT_VAR = 'fatjet_pt'
VAR_CHOICES = {'fatjet_eta', 'fatjet_pt', 'fatjet_mass'}


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('datasets', nargs='+')
    parser.add_argument('-w', '--weights', required=True, help=_DENOM_HELP)
    parser.add_argument('--var', choices=VAR_CHOICES, default=DEFAULT_VAR, help=_VARS_HELP)
    parser.add_argument('-r', '--roc-discriminants', nargs='+', choices=DISC_CHOICES,
                        default=DEFAULT_DISCRIMS, metavar='DISCRIM', help=_ROC_DISC_HELP)
    parser.add_argument('-i', '--path-to-pt-eta-hists', type=Path, default='pt-eta-hists',
                        help=_PT_HISTS_HELP)
    parser.add_argument('-o', '--out-file', type=Path, default='roc2d.h5', help=_OUTFILE_HELP)
    parser.add_argument('--verbose', action='store_true', help=_VERBOSE_HELP)
    parser.add_argument('-p', '--pt-range', nargs=2, type=float, default=DEFAULT_PT_RANGE,
                        help=_PT_RANGE_HELP)

    return parser.parse_args()


# ==================================================================
# main()
# ==================================================================
def run():
    # Get args
    args = get_args()
    # ================
    # Prepare Output directory
    # ================
    # Get out-dir
    outfile = args.out_file
    outfile.parent.mkdir(parents=True, exist_ok=True)
    if(outfile.is_file()):
        outfile.unlink()
    # ================
    # Prepare fat-jet selectors
    # Must later pass the fat-jets datsets to them
    # ================
    pt_range = args.pt_range
    var_to_study = args.var
    mass_window = mass_window_higgs
    if var_to_study == 'fatjet_mass':
        mass_window = loose_mass_window
    dijet_selector = fatjet_mass_window_pt_range(pt_range, mass_window)
    higgs_selector = fatjet_mass_window_pt_range_truth_match(pt_range, mass_window,
                                                             {'GhostHBosonsCount': 1})
    top_selector = fatjet_mass_window_pt_range_truth_match(pt_range, mass_window,
                                                           {'GhostTQuarksFinalCount': 1})
    #top_selector = fatjet_mass_window_pt_range(pt_range, mass_window_higgs,
    #                                           var_to_study=var_to_study)
    # Selecting fat-jet based on flavour of 3 subjets that make it
    flavours = ['all', '2b+0c', '1b+1c', '1b+0c', '2c+0b', '1c+0b', '0b+0c']
    flav_selectors = []
    for flavour in flavours:
        flav_selectors.append(truth_match_hadron_flav(flavour, final_count=True))

    # Parallelise running all samples
    from multiprocessing import Process
    # Start the processes
    p1 = Process(target=run_process, args=('dijet', args, dijet_selector, flav_selectors, flavours))
    p1.start()
    p2 = Process(target=run_process, args=('higgs', args, higgs_selector, flav_selectors, flavours))
    p2.start()
    p3 = Process(target=run_process, args=('top', args, top_selector, flav_selectors, flavours))
    p3.start()
    # Join the processes
    p1.join()
    p2.join()
    p3.join()


# ================
# Process and save output per process
# ================
def run_process_wrapper(params):
    process = params[0]
    in_args = params[1]
    selector = params[2]
    flav_selectors = params[3]
    flavs = params[4]
    return run_process(process, in_args, flav_selectors, flavs)


# ==================================================================
# Function to run a process, which means getting the histogram with appropriate settings
# and initiate save output
# ==================================================================
def run_process(process, args, selection, flav_selectors, flavours, signal='higgs'):
    # ================
    # Get all args
    # ================
    # Be verbose?
    verbose = args.verbose
    # Get the destination directory
    outfile = args.out_file
    # The pt_eta_histos file to output to then use for reweights
    pt_eta_hists_file = args.path_to_pt_eta_hists/'jet_pt_eta.h5'
    # Overall sample weights file
    sample_weights_file = args.weights
    # Get the sample weights from file
    with open(sample_weights_file, 'r') as ds_wts_file:
        sample_weights = get_ds_weights_dict(ds_wts_file)
    # The sample datasets to process
    datasets = args.datasets
    # get only datasets relevant to the current process
    valid_datasets = [ds for ds in datasets
                      if PROC_SELECTORS[process](get_dsid(ds))
                      and np.isfinite(sample_weights[get_dsid(ds)])]
    # Discriminants to Study
    discrims_of_interest = args.roc_discriminants
    # pT range
    pt_range = args.pt_range
    # Variable to plot against discriminant in 2D Roc
    var = args.var

    # ================
    # Process the samples
    # ================
    if verbose:
        print(f'Processing {process} samples')
    # Loop over all discriminants
    for discrim_name in discrims_of_interest:
        if verbose:
            print(f'running {discrim_name}')
        # ================
        # Initialize Stuff
        # ================
        # Initialize histogram (numpy bin-vals)  with weight = weight
        hist = 0
        # Initialize histogram with weight = weights^2  to be used for error calculations
        hist_with_wt2 = 0
        # Initialize discriminant to histos map
        discrim_hists_map = {}

        # Initialise LRJ flavour composition to histo maps
        flav_to_hist_map = {flavour: hist for flavour in flavours}
        flav_to_hist_wt2_map = {flavour: hist_with_wt2 for flavour in flavours}

        # Get pre-defined edges for discrim Histograms, and add overflow and underflow bins
        edges = get_edges(discrim_name, var)
        edges_overflow = [np.concatenate([[-np.inf], e, [np.inf]]) for e in edges]
        # Get the discriminant
        discrim_getter = VARIABLE_GETTERS[discrim_name]
        # Loop over datasets for this process
        for ds in valid_datasets:
            if args.verbose:
                print(f'running on {ds} as {process}')

            # Get the histogram for this dataset (a numpy array with bin-values)
            (this_dsid_flav_to_hist_map,
                this_dsid_flav_to_hist_wt2_map) = get_hist(ds, var, discrim_getter,
                                                           edges_overflow,
                                                           pt_eta_hists_file,
                                                           selection, process,
                                                           sample_weights, flav_selectors,
                                                           flavours)

            # for each dataset, we process all flavours and add histos for each one
            for flavour in flavours:
                # Sum all histograms for this process from all flavours
                flav_to_hist_map[flavour] += this_dsid_flav_to_hist_map[flavour]
                flav_to_hist_wt2_map[flavour] += this_dsid_flav_to_hist_wt2_map[flavour]

        # The signal composition is trivial. Change all flavours to be equal no flavour selection
        if(process == signal):
            for flavour in flavours:
                flav_to_hist_map[flavour] = flav_to_hist_map['all']
                flav_to_hist_wt2_map[flavour] = flav_to_hist_wt2_map['all']

        # Make a discrim name to histo map
        discrim_hists_map[discrim_name] = (flav_to_hist_map, flav_to_hist_wt2_map)

        # Save the histogram to file
        save_hist(process, discrim_hists_map, edges, outfile, pt_range, flavours)


# ==================================================================
# Function which gets discriminant histogram bin-value for each sample dataset
# ==================================================================
def get_hist(ds, var, discrim_getter, edges_overflow, pt_eta_hists_file, selection, process,
             sample_weights, flav_selectors, flavours):

    # ==================================================================
    # Getting Weights for the histogram
    # ==================================================================
    (pt_reweighting, eta_reweighting,
        pt_edges, eta_edges) = get_pt_eta_reweighting(pt_eta_hists_file,
                                                      process, get_edges=True)

    # Initialize histogram (numpy bin-vals)  with weight = weight
    hist = 0
    # Initialize histogram with weight = weights^2  to be used for error calculations
    hist_with_wt2 = 0
    # Initialise LRJ flavour composition to histo maps
    flav_to_hist_map = {flavour: hist for flavour in flavours}
    flav_to_hist_wt2_map = {flavour: hist_with_wt2 for flavour in flavours}

    # Loop over H5 files for sample
    for fpath in glob(f'{ds}/*.h5'):
        with File(fpath, 'r') as h5file:
            # Get fat-jets
            fat_jets = np.asarray(h5file['fat_jet'])
            # Get fat-jets pT array
            pt = fat_jets['pt']
            # Get the bin of each pT entry (length = pt array length)
            # if pt = (pt1,pt2) & pt1 in bin 1, pt2 in bin 5, then indicies = (1,5)
            indices = np.digitize(pt, pt_edges) - 1
            # Overall event weight, for dijet = L*XS*filt_Eff / N_{MC}, 1 otherwise
            sample_weight = sample_weights[get_dsid(ds)]
            # Re-define weight as the qcd/proc ratio and re-size it to match pt array size
            # if ratio = [r1,r2,r3,r4,r5] and indices=[3,3,2,4,1,2,2,1,1,4,4] (size = pt.size())
            # weight = [r3,r3,r2,r4,r1,r2,r2,r1,r1,r4,r4] (size =pt.size())
            weight = pt_reweighting[indices]*sample_weight*fat_jets['mcEventWeight']
            # get the array of discriminant values for each fat-jet
            disc = discrim_getter(h5file)
            # apply the selector function to the fat-jet
            sel = selection(fat_jets)
            # get the variable we want to plot discriminant against
            var_to_plot = VARIABLE_GETTERS[var](h5file)

            subjet1 = np.asarray(h5file['subjet_VRGhostTag_1'])
            subjet2 = np.asarray(h5file['subjet_VRGhostTag_2'])
            subjet3 = np.asarray(h5file['subjet_VRGhostTag_3'])
            subjets = [subjet1, subjet2, subjet3]

            for idx, flavour in enumerate(flavours):
                flavour_sel = flav_selectors[idx](fat_jets, subjets)
                overall_sel = sel & flavour_sel
                # Make input array (2D array, each entry is x,y values, z is number of jets)
                # [[pT1,disc1],[pT2,disc2],...]
                input_arr = np.stack((disc[overall_sel], var_to_plot[overall_sel])).T
                # Make the histogram and add them up from different h5 files in the dataset
                flav_to_hist_map[flavour] += np.histogramdd(input_arr, edges_overflow,
                                                            weights=weight[overall_sel])[0]
                flav_to_hist_wt2_map[flavour] += np.histogramdd(input_arr, edges_overflow,
                                                                weights=weight[overall_sel]**2)[0]
    # return the total histogram for the dataset
    return flav_to_hist_map, flav_to_hist_wt2_map


# ==================================================================
# Function which writes out contents of dict (discrim_name,[2d_histo_wt,2d_histo_wt2])
# to an h5 file,wit structure:
# pt_range_grp -> discrim_grp -> flav_grp -> proc_grp (with limits_attr) -> histos
# ==================================================================
def save_hist(process, discrim_hists_map, edges, outfile, pt_range, flavours):

    # Open output file in append mode (because we will fill it once per discrim  per process)
    with File(outfile, 'a') as out_file:

        # Make a group for pT range to hold 1 group per discrim to hold 1 dataset per process
        pt_str_gev = '_'.join('{:.0f}'.format(x*1e-3) for x in pt_range)
        pt_range_group = out_file.require_group(f'/{pt_str_gev}')
        pt_range_group.attrs['pt_range'] = pt_range

        limits = [(axis_edges.min(), axis_edges.max()) for axis_edges in edges]
        discrim_name = [*discrim_hists_map][0]
        # Make a group for each discriminant
        disc_group = pt_range_group.require_group(discrim_name)

        for flavour in flavours:
            flav_group = disc_group.require_group(flavour)
            # Create a group for each process
            proc_group = flav_group.require_group(process)
            # Make group datasets for hists with normal weights and one with weights^2
            proc_group.create_dataset('hist_wt',
                                      data=discrim_hists_map[discrim_name][0][flavour],
                                      compression='gzip')
            proc_group.create_dataset('hist_wt2',
                                      data=discrim_hists_map[discrim_name][1][flavour],
                                      compression='gzip')
            # Make a group attribute
            try:
                proc_group.attrs['limits']
            except:
                proc_group.attrs['limits'] = limits


# ==================================================================
# Function to get bining for x and y axes of the 2D histogram
# ==================================================================
def get_edges(discrim_name, var):
    default_discrim_edges = VARIABLE_EDGES[discrim_name]
    default_var_edges = VARIABLE_EDGES[var]
    edges = [np.linspace(default_discrim_edges[0],
                         default_discrim_edges[-1], 5000),
             default_var_edges]
    return edges


if __name__ == '__main__':
    run()
