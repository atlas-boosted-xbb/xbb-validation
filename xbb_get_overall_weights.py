#!/usr/bin/env python3

"""
Gets the number of events in each sample , for normalization stuffs

Returns a json file with DSID: NEvents entries. NEvents for QCD, SoW for non-QCD

datasets == H5 datasets
"""

from argparse import ArgumentParser
from h5py import File
from glob import glob
import numpy as np
import os.path
from pathlib import Path
import json

from collections import Counter
from xbb.common import get_dsid
from xbb.selectors import is_any_dijet, is_dijet
from xbb.cross_section import CrossSections
# ==================================================================
# Get arguments from CL
# ==================================================================
_help_datasets = 'All H5 datasets to run over, uses dataset short-name. Wildcards-comaptible'
_XS_HELP = 'The .txt file containing space separated info on XS and filter eff'


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('datasets', nargs='+', help=_help_datasets)
    parser.add_argument('-x', '--cross-sections', required=True, help=_XS_HELP)
    parser.add_argument('-o', '--out-dir', type=Path, default='./')
    return parser.parse_args()

# ==================================================================
# Functio retrieves number of events or sum of weights from H5 file metadata
# ==================================================================
def run():
    args = get_args()
    xs_file = args.cross_sections
    # make a dict with elements as keys and their counts are values
    counts = Counter()

    ds_to_weight_map = {}
    for ds in args.datasets:
        dsid = get_dsid(ds)
        h5_to_mcEventWeight_map = {}
        for fpath in glob(f'{ds}/*.h5'):
            with File(fpath,'r') as h5file:
                #for H5 file in each dataset get a count of the number of events 
                #returns actual Number of events for dijet samples, while SoW for others
                counts[dsid] += get_counts(h5file, is_dijet=is_dijet("pythia",dsid)) # only pythia needs nEventsProcessed 

        ds_wt = get_overall_sample_weight(dsid,counts,xs_file,is_any_dijet(dsid))
        ds_to_weight_map[dsid] = ds_wt

    # Dump out the count into a json file with fixed name
    outdir = args.out_dir
    # create output directory with parents if it is not there
    if(not os.path.isdir(outdir)):
        outdir.mkdir(parents=True)
    weights_file = outdir.joinpath("overall_sample_weights.json")
    with open(weights_file, "w+") as output_weights_file:
        json.dump(dict(ds_to_weight_map), output_weights_file, indent=2)


def get_counts(h5file, is_dijet):
    key = 'nEventsProcessed' if is_dijet else 'sumOfWeights'

    return float(np.sum(h5file['metadata'][key]))


def get_overall_sample_weight(dsid, denom, xs_file, is_dijet):
    if(is_dijet):
        # Prepare overall normalization to dijet histograms
        with open(xs_file, 'r') as xsec_file:
            # Initialize a CrossSection class object (will extract normalization from it)
            xsecs = CrossSections(xsec_file, denom)
        ds_weight = xsecs.get_weight(dsid)
    else:
        ds_weight = 1.

    return ds_weight


if __name__ == '__main__':
    run()
