#!/usr/bin/env python3

"""
Make histograms of the jet pt and eta spectra and saving jet_pt_eta.h5 for later reweighting

Returns a file jet_pt_eta.h5 which contains:
    - 3 Groups: Higgs, Top and dijet
      - Each group (a.k.a proc) contains:
        - A dataset with pT spectrum bin-values, called 'pt_hist'
        - A dataset with pT spectrum bin-edges, called 'pt_edges'
        - A dataset with eta spectrum bin-values, called 'eta_hist'
        - A dataset with eta spectrum bin-edges, called 'eta_edges'
    - A Group of datasets corresponding to samples summed up to make spectrum
    - A Group with ratio QCD['hist']/proc['hist'], called 'dijet_{process}_ratio'
      - A dataset with ratio bin-values, called 'pt_hist'
      - A dataset with ratio bin-edges = pT bin-edges, called 'pt_edges'
      - A dataset with ratio bin-values, called 'eta_edges'
      - A dataset with ratio bin-edges = pT bin-edges, called 'eta_edges'
ds = dataset
processes below often refer to different generation settings. E.G. different pT slices in dijets or
G* masses in Higgs
"""
from argparse import ArgumentParser
from h5py import File
from glob import glob
import numpy as np
import json
import os
from sys import stderr
from pathlib import Path

from xbb.common import dhelp
from xbb.common import (get_ds_weights_dict, get_dsid, get_name,
                        get_formatted_name, get_pt_eta_reweighting)
from xbb.selectors import PROC_SELECTORS
from xbb.selectors import (mass_window_higgs, fatjet_mass_window_pt_range_truth_match,
                           fatjet_mass_window_pt_range)
from xbb.mpl import (Canvas, xlabdic, ylabdic, helvetify,
                     add_atlas_label, add_kinematic_acceptance)
# ==================================================================
# Get arguments from CL
# ==================================================================
_VERBOSE_HELP = 'Turn on Verbose run'
_WEIGHTS_HELP = 'The .json file containing DSID:Sample Weight Map '
_OUTDIR_HELP = dhelp('The destination directory where all files are saved')


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('datasets', nargs='+')
    parser.add_argument('-v', '--verbose', action='store_true', help=_VERBOSE_HELP)
    parser.add_argument('-w', '--weights', required=True, help=_WEIGHTS_HELP)
    parser.add_argument('-o', '--out-dir', default='pt-eta-hists', type=Path, help=_OUTDIR_HELP)
    return parser.parse_args()


# ==================================================================
# main()
# ==================================================================
def run():
    args = get_args()
    # ================
    # Prepare Output directory
    # ================
    # Get out-dir
    out_dir = args.out_dir
    # If requested destination directory doesn't exist, create it
    out_dir.mkdir(parents=True, exist_ok=True)
    # ================
    # Prepare fat-jet selectors
    # Must later pass the fat-jets datsets to them
    # ================
    dijet_selector = fatjet_mass_window_pt_range((250e3, np.inf), mass_window_higgs)
    higgs_selector = fatjet_mass_window_pt_range_truth_match((250e3, np.inf), mass_window_higgs,
                                                             {'GhostHBosonsCount': 1})
    top_selector = fatjet_mass_window_pt_range_truth_match((250e3, np.inf), mass_window_higgs,
                                                           {'GhostTQuarksFinalCount': 1})
    # ================
    # Running different processes
    # ================
    # Have to run all un-reeighted first then use them for re-weighting
    run_process('dijet', args, dijet_selector, reweight=False)
    run_process('higgs', args, higgs_selector, reweight=False)
    run_process('top', args, top_selector, reweight=False)
    run_process('higgs', args, higgs_selector, reweight=True)
    run_process('top', args, top_selector, reweight=True)


# ==================================================================
# Function to run process -- i.e. getting the histos with appropriate settings, drawing, saving them
# ==================================================================
def run_process(process, args, selection, reweight=False):
    # ================
    # Get all args
    # ================
    # Be verbose?
    verbose = args.verbose
    # Get the destination directory
    out_dir = args.out_dir
    # The pt_eta_histos file to output to then use for reweights
    pt_eta_hists_file = f'{out_dir}/jet_pt_eta.h5'
    # Overall sample weights file
    sample_weights_file = args.weights
    # Get the sample weights from file
    with open(sample_weights_file, 'r') as ds_wts_file:
        sample_weights = get_ds_weights_dict(ds_wts_file)
    # The sample datasets to process
    datasets = args.datasets
    # get only datasets relevant to the current process
    valid_datasets = [ds for ds in datasets
                      if PROC_SELECTORS[process](get_dsid(ds))
                      and np.isfinite(sample_weights[get_dsid(ds)])]

    # ================
    # Initialize Stuff
    # ================
    # Initialize histograms and process->histo map
    pT_hist = 0
    eta_hist = 0
    dijet_proc_ratio_hist = 0
    proc_hist_map = {}

    # ================
    # Make histograms for process
    # ================
    # Loop over valid datastes
    for ds in valid_datasets:
        # Get dsid
        dsid = get_dsid(ds)
        if verbose:
            print(f'running on {ds} as {process}')
        # make edges and add an underflow and overflow bins for any pT/eta outside the edges
        pt_edges = np.concatenate([[-np.inf], np.linspace(250e3, 3e6, 51), [np.inf]])
        eta_edges = np.concatenate([[-np.inf], np.linspace(-2, 2, 51), [np.inf]])
        # Get the histogram for this DSID
        this_dsid_hist_pt, this_dsid_hist_eta = get_hist(ds, process,
                                                         pt_edges, eta_edges,
                                                         pt_eta_hists_file,
                                                         selection,
                                                         sample_weights,
                                                         reweight=reweight)
        # sum all histograms for process into a total pT/eta histogram
        pT_hist += this_dsid_hist_pt
        eta_hist += this_dsid_hist_eta
        # make a short representive dataset name (e.g. jz_{jzslice}) using regex
        name = get_name(ds)
        # fill dictionary with (key,value) = (name of dataset, histogram)
        proc_hist_map[name] = np.array([this_dsid_hist_pt, this_dsid_hist_eta])

    # ================
    # Drawing the histos
    # ================
    if(reweight):
        outplot_name = f'{process}_reweight.pdf'
    else:
        outplot_name = f'{process}.pdf'

    # Draw the pT/eta histogram (overlaid with individual constituent processes) in pdf
    # We draw all histos, reweighted or not
    draw_hist(pT_hist, eta_hist, pt_edges, eta_edges,
              out_dir, proc_hist_map, file_name=outplot_name)

    # ================
    # Saving the histos
    # ================
    if(reweight):
        pt_reweighting, eta_reweighting = get_pt_eta_reweighting(pt_eta_hists_file, process)
        # We only save the non-reweighted histogram to the H5 file
        # make a qcd/proc ratio histo, with edges = pT/eta edges
        (dijet_proc_ratio_hist_pt,
            dijet_proc_ratio_hist_eta) = get_dijet_proc_ratio_hist(pt_reweighting, eta_reweighting,
                                                                   pt_edges, eta_edges)
        # save the ratio histogram in the jet_pt_eta.h5 file
        save_hist(dijet_proc_ratio_hist_pt, dijet_proc_ratio_hist_eta,
                  pt_edges, eta_edges, pt_eta_hists_file,
                  'dijet_'+process+'_ratio')
    else:
        # if we haven't been re-weighting, save pT/eta histogram for the sample to jet_pt_eta.h5.
        save_hist(pT_hist, eta_hist, pt_edges, eta_edges, pt_eta_hists_file, process, proc_hist_map)


# ==================================================================
# Function to make the histogram itself for a given dataset
# ==================================================================
def get_hist(ds, process, pt_edges, eta_edges, pt_eta_hists_file,
             selection, sample_weights, reweight=False):

    # Initialize the histograms
    pt_hist = 0
    eta_hist = 0
    # open all h5 files in dataset
    for fpath in glob(f'{ds}/*.h5'):
        dsid = get_dsid(ds)
        with File(fpath, 'r') as h5file:
            # Get Fatjets
            fat_jets = np.asarray(h5file['fat_jet'])
            # Get fat-jet pT array
            pt = fat_jets['pt']
            eta = fat_jets['eta']
            # Get the mc Event weight for each fat-jet * (XS weight (in dijet) or 1 (in non-dijet) )
            weight_pT = h5file['fat_jet']['mcEventWeight']*sample_weights[dsid]
            weight_eta = h5file['fat_jet']['mcEventWeight']*sample_weights[dsid]

            # If we are reweighting, then we are dividing 2 histograms that we already saved
            if reweight:

                # Getting re-weights for the histogram
                pt_reweighting, eta_reweighting = get_pt_eta_reweighting(pt_eta_hists_file, process)
                # Get the bin of each pT/eta entry (length = pt array length)
                # if pt = (pt1,pt2) & pt1 in bin 1, pt2 in bin 5, then indicies = (1,5)
                indices_pt = np.digitize(pt, pt_edges) - 1
                indices_eta = np.digitize(eta, eta_edges) - 1
                # re-define weight as the qcd/proc ratio and re-size it to match pT/eta array size
                # if ratio = [r1,r2,r3,r4] and indices=[3,3,2,4,1,2,2,1,1,4,4] (size=pT/eta.size())
                # weight = [r3,r3,r2,r4,r1,r2,r2,r1,r1,r4,r4] (size =pT/eta.size())
                weight_pT *= pt_reweighting[indices_pt]
                weight_eta *= eta_reweighting[indices_eta]

            # turn the weight into a numpy array and multiply by any extra weights in args
            mega_weights_pT = np.array(weight_pT, dtype=np.longdouble)
            mega_weights_eta = np.array(weight_eta, dtype=np.longdouble)
            # apply the selector function to the fat-jet
            sel = selection(fat_jets)

            # make the numpy histogram and get the bin-values from it (element [0])
            # filtering pT/eta array uses numpy bool indexing
            pt_hist += np.histogram(pt[sel], pt_edges, weights=mega_weights_pT[sel])[0]
            eta_hist += np.histogram(eta[sel], eta_edges, weights=mega_weights_eta[sel])[0]

            # check if any elements of the histograms are nan and report the files with issues
            if (np.any(np.isnan(pt_hist)) or np.any(np.isnan(eta_hist))):
                stderr.write(f'{fpath} has nans\n')
    if(isinstance(pt_hist, int) and isinstance(eta_hist, int)):
        pt_hist = np.zeros(len(pt_edges)-1)
        eta_hist = np.zeros(len(eta_edges)-1)
    return pt_hist, eta_hist


# ==================================================================
# Function to draw a histogram and save it in PDF -- called once per process
# ==================================================================
def draw_hist(pt_hist, eta_hist, pt_edges, eta_edges,
              out_dir, proc_hist_dict={}, file_name='dijet.pdf'):

    # ================
    # Organise output
    # ================
    if not os.path.isdir(out_dir/'pT'):
        os.mkdir(out_dir/'pT')
    if not os.path.isdir(out_dir/'eta'):
        os.mkdir(out_dir/'eta')
    # ================
    # Draw pT
    # ================
    centers_pt = 1e-6 * (pt_edges[1:] + pt_edges[:-1]) / 2
    gev_per_bin = (centers_pt[2] - centers_pt[1]) * 1e3

    with Canvas(f'{out_dir}/pT/{file_name}') as can:
        add_atlas_label(can.ax, x=0.05, y=0.95, internal=True, vshift=0.05)
        add_kinematic_acceptance(can.ax, y=0.75, pt_range="250_inf", offset=0.08)
        # plot the total histogram
        can.ax.plot(centers_pt, pt_hist)
        can.ax.set_yscale('log')
        can.ax.set_ylabel(f'jets * fb / {gev_per_bin:.0f} GeV', **ylabdic())
        can.ax.set_xlabel(r'Fat Jet $p_{\rm T}$ [TeV]', **xlabdic())
        maxval = can.ax.get_ylim()[1]
        can.ax.set_ylim(0.1, maxval)
        # plot constituent processes histograms from dictionary
        for proc_name, bin_vals in proc_hist_dict.items():
            pt_binvals = bin_vals[0]
            # legends can be ugly,terse or pretty (pretty is e.g. $M_G = X GeV$)
            can.ax.plot(centers_pt, pt_binvals, label=get_formatted_name(proc_name, 'terse'))
        can.ax.legend(ncol=2)
    # ================
    # Draw Eta
    # ================
    centers_eta = (eta_edges[1:] + eta_edges[:-1]) / 2
    units_per_bin = (centers_eta[2] - centers_eta[1])

    with Canvas(f'{out_dir}/eta/{file_name}') as can:
        add_atlas_label(can.ax, x=0.05, y=0.95, internal=True, vshift=0.05)
        add_kinematic_acceptance(can.ax, y=0.75, pt_range="250_inf", offset=0.08)
        # plot the total histogram
        can.ax.plot(centers_eta, eta_hist)
        can.ax.set_yscale('log')
        can.ax.set_ylabel(f'jets * fb / {units_per_bin:.2f} Units', **ylabdic())
        can.ax.set_xlabel(r'Fat Jet $\eta$', **xlabdic())
        maxval = can.ax.get_ylim()[1]
        can.ax.set_ylim(0.1, maxval)
        # plot constituent processes histograms from dictionary
        for proc_name, bin_vals in proc_hist_dict.items():
            eta_binvals = bin_vals[1]
            # legends can be ugly,terse or pretty (pretty is e.g. $M_G = X GeV$)
            can.ax.plot(centers_eta, eta_binvals, label=get_formatted_name(proc_name, 'terse'))
        can.ax.legend(ncol=2)


# ==================================================================
# Function to save histograms to jet_pt_eta.h5 file with appropriate grouping by process
# This is called once per process, and prcoess name is fed as group_name
# ==================================================================
def save_hist(pt_hist, eta_hist, pt_edges, eta_edges,
              pt_eta_hists_file, group_name, proc_hist_dict={}):

    with File(pt_eta_hists_file, 'a') as h5file:
        # create a group named after process
        hist_group = h5file.create_group(group_name)
        # Insiide the group
        # create 2 datasets per variable , one for total hist bin-vals and other for bin-edges
        # we need a bit of a hack here: float128 (longdouble) doesn't
        # get stored properly by h5py as of version 2.8
        hist_group.create_dataset('pt_hist', data=pt_hist, dtype=float)
        hist_group.create_dataset('pt_edges', data=pt_edges)
        hist_group.create_dataset('eta_hist', data=eta_hist, dtype=float)
        hist_group.create_dataset('eta_edges', data=eta_edges)
        # save a nested group which holds the bin-vals for the different constituent processes
        for proc_name, bin_vals in proc_hist_dict.items():
            pt_binvals = bin_vals[0]
            eta_binvals = bin_vals[1]
            proc_grp = hist_group.require_group('processes')
            proc_grp.create_dataset(proc_name+'_pt', data=pt_binvals)
            proc_grp.create_dataset(proc_name+'_eta', data=eta_binvals)


# ==================================================================
# Function to return numpy histogram bin-values for qcd/proc ratios, binned using pT/eta binning
# ==================================================================
def get_dijet_proc_ratio_hist(pt_reweighting, eta_reweighting, pt_edges, eta_edges):
    pt_ratio_hist = 0
    eta_ratio_hist = 0
    # use trick so that data is just an array of size = edges, so every bin gets only "1 entry"
    # scale entry by the ratio, so that bin-values are the ratios
    pt_ratio_hist += np.histogram(pt_edges[:-1], pt_edges, weights=pt_reweighting)[0]
    eta_ratio_hist += np.histogram(eta_edges[:-1], eta_edges, weights=eta_reweighting)[0]

    return pt_ratio_hist, eta_ratio_hist


if __name__ == '__main__':
    run()
