import numpy as np


def all_events(ds):
    return np.ones_like(ds, dtype=bool)

def rm_large_weights(ds):
    weight = ds['mcEventWeight']
    return (weight < 10)

def truth_match(truth_label):
    def selector(ds):
        return ds[truth_label] == 1
    return selector

def mass_window_top(ds):
    return ds['mass'] > 60e3


def mass_window_higgs(ds):
    fat_mass = ds['mass']
    return (fat_mass > 76e3) & (fat_mass < 146e3)

def loose_mass_window(ds):
    fat_mass = ds['mass']
    return (fat_mass > 20e3) & (fat_mass < 500e3)

# combined selectors

def fatjet_mass_window_pt_range(pt_range, mass_window):
    def selector(ds):
        window = mass_window(ds)
        fat_pt = ds['pt']
        pt_window = (fat_pt > pt_range[0]) & (fat_pt < pt_range[1])
        return window & pt_window
    return selector

def truth_match_mass_window_higgs(truth_label):
    def selector(ds):
        truth = ds[truth_label] == 1
        window = mass_window_higgs(ds)
        return truth & window
    return selector

def fatjet_mass_window_pt_range_truth_match(pt_range, mass_window, truth_labels):
    def selector(ds):
        window = mass_window(ds)
        truth = all_events(ds)
        for label, condition in truth_labels.items():
            extra = ds[label] == condition
            truth = truth & extra
        fat_pt = ds['pt']
        pt_window = (fat_pt > pt_range[0]) & (fat_pt < pt_range[1])
        return window & pt_window & truth
    return selector
# process selectors

def is_any_dijet(dsid, restricted=True):
    return is_dijet_pythia(dsid,restricted) or is_dijet_herwig(dsid,restricted)

def is_dijet(generator, dsid, restricted=True):
    if generator == "pythia": return is_dijet_pythia(dsid,restricted)
    if generator == "herwig": return is_dijet_herwig(dsid,restricted)

def is_dijet_pythia(dsid, restricted=True):
    if restricted:
        return 361023 <= dsid <= 361028
    return 361022 <= dsid <= 361032

def is_dijet_herwig(dsid, restricted=True):
    if restricted:
        return 364903 <= dsid <= 364908
    return 364902 <= dsid <= 364912

def is_ditop(dsid, restricted=False):
    return 301322 <= dsid <= 301335

    return dsid == 0

def is_dihiggs(dsid, restricted=False): # is there a reason to ever restrict this range?
    return ((301488 <= dsid <= 301507) or (305776 <= dsid <= 305780))

def is_data(dsid, restricted=False):
    return dsid == 0

def is_wz(dsid, restricted=False):
    return 301254 <= dsid <= 301287


def truth_match_hadron_flav(composition, final_count=False):
    # Getting Bools to select different compositions of the LRJ
    def selector(fatjet_ds, subjets_datasets):
        # Numpy Array to hold number of B-JETS per LRJ
        NumBHadrons = np.zeros_like(fatjet_ds, dtype=int)
        # Numpy Array to hold number of C-JETS per LRJ
        NumCHadrons = np.zeros_like(fatjet_ds, dtype=int)
        # Loop over subjets (typically 3 subjets)
        for subjet_ds in subjets_datasets:
            # If user wants to use GhostMatched FinalCount Variables
            if(final_count):
                GhostCHadronsFinalCount = subjet_ds['GhostCHadronsFinalCount']
                GhostBHadronsFinalCount = subjet_ds['GhostBHadronsFinalCount']
                # When no Subjet is found, these variables return -1 in the H5
                not_nan_B = GhostBHadronsFinalCount != -1
                not_nan_C = GhostCHadronsFinalCount != -1
                # Count subjets of each flavour, if subjet is not NaN
                NumBHadrons[not_nan_B] += GhostBHadronsFinalCount[not_nan_B]
                NumCHadrons[not_nan_C] += GhostCHadronsFinalCount[not_nan_C]

            # If user wants to use Truth Labelling for each subjet
            else:
                # Get the abosolute value of the PDG ID of the subjet
                label = np.absolute(subjet_ds['HadronConeExclTruthLabelID'])
                # Find out if it's a B or C
                where_b_hadrons_are = (label == 5)
                where_c_hadrons_are = (label == 4)
                # Increment where B or C hadrons are found
                NumBHadrons[where_b_hadrons_are] += 1
                NumCHadrons[where_c_hadrons_are] += 1

        # Map different compositions fed by user to conditons on Num of subjets
        FLAV_COMP_TO_SELECTION = {

            'all': (),
            '2b+0c': (NumBHadrons == 2, NumCHadrons != -123),
            '1b+1c': (NumBHadrons == 1, NumCHadrons >= 1),
            '1b+0c': (NumBHadrons == 1, NumCHadrons == 0),
            '2c+0b': (NumBHadrons == 0, NumCHadrons >= 2),
            '1c+0b': (NumBHadrons == 0, NumCHadrons == 1),
            '0b+0c': (NumBHadrons == 0, NumCHadrons == 0),
        }
        # Get the selection for the asked composition
        selections = FLAV_COMP_TO_SELECTION[composition]
        # Start by reseting selection (accept all LRJs)
        truth = all_events(fatjet_ds)
        # Then apply criteria to B and C hadron count
        if(len(selections) != 0):
            NumBSelec = selections[0]
            NumCSelec = selections[1]
            truth = truth & NumBSelec & NumCSelec
        return truth

    return selector


PROC_SELECTORS = {
    'higgs': is_dihiggs,
    'dijet': is_dijet_pythia, #pythia by default
    'dijet_herwig': is_dijet_herwig,
    'top': is_ditop,
    'z': is_wz,
    'data': is_data
}

EVENT_LABELS = {
    'higgs': 'GhostHBosonsCount',
    'top': 'GhostTQuarksFinalCount',
    'z': 'GhostZBosonsCount'
}
