# ====================
# variable getters #
# ====================
import numpy as np

# Different Subjet collection names
# Used to retrieve the nth subjet of any collection
SJ_VR = 'subjet_VR_{}'
SJ_GHOST = 'subjet_VRGhostTag_{}'
SJ_FR = 'subjet_FR_{}'


# ==================================================
# Tagger Discriminants
# ==================================================
def make_xbb_v1_getter(ftop=0.25):
    def get_xbb(h5file, ftop=ftop):
        fj = h5file['fat_jet']['Xbb202006_Higgs', 'Xbb202006_QCD', 'Xbb202006_Top']
        num = fj['Xbb202006_Higgs']
        denom_qcd = fj['Xbb202006_QCD'] * (1 - ftop)
        denom_top = fj['Xbb202006_Top'] * ftop
        denom = denom_qcd + denom_top
        valid = (denom != 0.0) & (num != 0.0) & (np.isfinite(num))
        ret_vals = np.empty_like(num)
        ret_vals[valid] = np.log(num[valid] / denom[valid])
        ret_vals[~valid] = -15.0
        return ret_vals
    return get_xbb
# def make_xbb_v1_getter(ftop=0.25):
#     def get_xbb(h5file,ftop=ftop):
#         fj = h5file['fat_jet']['SubjetBScore_Higgs', 'SubjetBScore_QCD', 'SubjetBScore_Top']
#         num = fj['SubjetBScore_Higgs']
#         denom_qcd = fj['SubjetBScore_QCD'] * (1 - ftop)
#         denom_top = fj['SubjetBScore_Top'] * ftop
#         denom = denom_qcd + denom_top
#         valid = (denom != 0.0) & (num != 0.0) & (np.isfinite(num))
#         ret_vals = np.empty_like(num)
#         ret_vals[valid] = np.log(num[valid] / denom[valid])
#         ret_vals[~valid] = -15.0
#         return ret_vals
#     return get_xbb


def make_xbb_v2_getter(ftop=0.25):
    def get_xbb(h5file, ftop=ftop):
        fj = h5file['fat_jet']['Xbb2020v2_Higgs', 'Xbb2020v2_QCD', 'Xbb2020v2_Top']
        num = fj['Xbb2020v2_Higgs']
        denom_qcd = fj['Xbb2020v2_QCD'] * (1 - ftop)
        denom_top = fj['Xbb2020v2_Top'] * ftop
        denom = denom_qcd + denom_top
        valid = (denom != 0.0) & (num != 0.0) & (np.isfinite(num))
        ret_vals = np.empty_like(num)
        ret_vals[valid] = np.log(num[valid] / denom[valid])
        ret_vals[~valid] = -15.0
        return ret_vals
    return get_xbb


def make_xbb_v3_getter(ftop=0.25):
    def get_xbb(h5file, ftop=ftop):
        fj = h5file['fat_jet']['Xbb2020v3_Higgs', 'Xbb2020v3_QCD', 'Xbb2020v3_Top']
        num = fj['Xbb2020v3_Higgs']
        denom_qcd = fj['Xbb2020v3_QCD'] * (1 - ftop)
        denom_top = fj['Xbb2020v3_Top'] * ftop
        denom = denom_qcd + denom_top
        valid = (denom != 0.0) & (num != 0.0) & (np.isfinite(num))
        ret_vals = np.empty_like(num)
        ret_vals[valid] = np.log(num[valid] / denom[valid])
        ret_vals[~valid] = -15.0
        return ret_vals
    return get_xbb


def make_dl1r_getter(subjet=SJ_VR):
    def get_dl1(h5file, subjet=subjet):
        def dl1_sj(subjet, f=0.018):
            sj = h5file[subjet]['DL1r_pb', 'DL1r_pu', 'DL1r_pc']
            return sj['DL1r_pb'] / ((1-f) * sj['DL1r_pu'] + f * sj['DL1r_pc'])
        disc1 = dl1_sj(subjet.format(1))
        disc2 = dl1_sj(subjet.format(2))
        discrim_comb = np.stack([disc1, disc2], axis=1).min(axis=1)
        invalid = np.isnan(discrim_comb) | np.isinf(discrim_comb)
        discrim_comb[invalid] = 1e-15
        return np.log(np.clip(discrim_comb, 1e-30, 1e30))
    return get_dl1


def make_dl1_getter(subjet=SJ_VR):
    def get_dl1(h5file, subjet=subjet):
        def dl1_sj(subjet, f=0.08):  # this is still 0.08
            sj = h5file[subjet]['DL1_pb', 'DL1_pu', 'DL1_pc']
            return sj['DL1_pb'] / ((1-f) * sj['DL1_pu'] + f * sj['DL1_pc'])
        disc1 = dl1_sj(subjet.format(1))
        disc2 = dl1_sj(subjet.format(2))
        discrim_comb = np.stack([disc1, disc2], axis=1).min(axis=1)
        invalid = np.isnan(discrim_comb) | np.isinf(discrim_comb)
        discrim_comb[invalid] = 1e-15
        return np.log(np.clip(discrim_comb, 1e-30, 1e30))
    return get_dl1


def make_mv2_getter(subjet=SJ_VR):
    def get_mv2(h5file, subjet=subjet, discriminant='MV2c10_discriminant'):
        disc1 = h5file[subjet.format(1)][discriminant]
        disc2 = h5file[subjet.format(2)][discriminant]
        discrim_comb = np.stack([disc1, disc2], axis=1).min(axis=1)
        invalid = np.isnan(discrim_comb)
        discrim_comb[invalid] = -2.0
        return discrim_comb
    return get_mv2


def get_tau32(h5file):
    vals = 1.0 - h5file['fat_jet']['Tau32_wta']
    vals[~np.isfinite(vals)] = 0.0
    return vals


def get_jsstop(h5file):
    vals = h5file['fat_jet']['JSSTopScore']
    vals[~np.isfinite(vals)] = 0.0
    return vals


# ==================================================
# Kinematic Variables
# ==================================================
def get_fatjet_mass(h5file):
    return h5file['fat_jet']['mass']


def get_fatjet_pt(h5file):
    return h5file['fat_jet']['pt']


def get_fatjet_eta(h5file):
    return h5file['fat_jet']['eta']


def subjet_VRjet_dR(h5file, subjet):
    dR = h5file[subjet]['relativeDeltaRToVRJet']
    return dR


# ==================================================
# Dictionaries to allow getting any Variable from H5 file
# ==================================================
VARIABLE_GETTERS = {
    'dl1r': make_dl1r_getter(SJ_GHOST),
    'dl1r_FR': make_dl1r_getter(SJ_FR),
    'dl1r_VR': make_dl1r_getter(SJ_VR),
    'Xbb_V1_ftop0': make_xbb_v1_getter(ftop=0),
    'Xbb_V1_ftop010': make_xbb_v1_getter(ftop=0.1),
    'Xbb_V1_ftop1': make_xbb_v1_getter(ftop=1.0),
    'Xbb_V1_ftop050': make_xbb_v1_getter(ftop=0.5),
    'Xbb_V1_ftop025': make_xbb_v1_getter(ftop=0.25),
    'Xbb_V2_ftop0': make_xbb_v2_getter(ftop=0),
    'Xbb_V2_ftop1': make_xbb_v2_getter(ftop=1.0),
    'Xbb_V2_ftop050': make_xbb_v2_getter(ftop=0.5),
    'Xbb_V2_ftop025': make_xbb_v2_getter(ftop=0.25),
    'Xbb_V3_ftop0': make_xbb_v3_getter(ftop=0),
    'Xbb_V3_ftop010': make_xbb_v3_getter(ftop=0.1),
    'Xbb_V3_ftop1': make_xbb_v3_getter(ftop=1.0),
    'Xbb_V3_ftop050': make_xbb_v3_getter(ftop=0.5),
    'Xbb_V3_ftop025': make_xbb_v3_getter(ftop=0.25),
    'mv2': make_mv2_getter(),
    'mv2_FR': make_mv2_getter(SJ_FR),
    'tau32_top_vs_qcd': get_tau32,
    'jss_top_vs_qcd': get_jsstop,
    'fatjet_eta': get_fatjet_eta,
    'fatjet_pt': get_fatjet_pt,
    'fatjet_mass': get_fatjet_mass,
    'subjet_VRjet_dR': subjet_VRjet_dR,
}

VARIABLE_EDGES = {
    'dl1r': np.linspace(-10, 10, 1000),
    'dl1r_FR': np.linspace(-10, 10, 1000),
    'dl1r_VR': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop0': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop010': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop1': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop050': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop025': np.linspace(-10, 10, 1000),
    'Xbb_V2_ftop0': np.linspace(-10, 10, 1000),
    'Xbb_V2_ftop1': np.linspace(-10, 10, 1000),
    'Xbb_V2_ftop050': np.linspace(-10, 10, 1000),
    'Xbb_V2_ftop025': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop0': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop010': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop1': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop050': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop025': np.linspace(-10, 10, 1000),
    'mv2': np.linspace(-1, 1, 1000),
    'mv2_FR': np.linspace(-1, 1, 1000),
    'tau32_top_vs_qcd': np.linspace(0, 1, 1000),
    'jss_top_vs_qcd': np.linspace(0, 1, 1000),
    'fatjet_eta': np.linspace(-2, 2, 201),
    'fatjet_pt': np.linspace(250e3, 2500e3, 451),
    'fatjet_mass': np.linspace(50e3, 300e3, 401),
    'subjet_VRjet_dR': np.linspace(0, 3, 30),
    'flat': np.linspace(0, 1, 100),
}
# ==================================================
# Dictionaries to allow getting Discriminants Only from H5 files
# Slimmed versions from the general dictionaries
# ==================================================
DISCRIMINANT_GETTERS = {
    'dl1r': make_dl1r_getter(SJ_GHOST),
    'dl1r_FR': make_dl1r_getter(SJ_FR),
    'dl1r_VR': make_dl1r_getter(SJ_VR),
    'Xbb_V1_ftop0': make_xbb_v1_getter(ftop=0),
    'Xbb_V1_ftop010': make_xbb_v1_getter(ftop=0.1),
    'Xbb_V1_ftop1': make_xbb_v1_getter(ftop=1.0),
    'Xbb_V1_ftop050': make_xbb_v1_getter(ftop=0.5),
    'Xbb_V1_ftop025': make_xbb_v1_getter(ftop=0.25),
    'Xbb_V2_ftop0': make_xbb_v2_getter(ftop=0),
    'Xbb_V2_ftop1': make_xbb_v2_getter(ftop=1.0),
    'Xbb_V2_ftop050': make_xbb_v2_getter(ftop=0.5),
    'Xbb_V2_ftop025': make_xbb_v2_getter(ftop=0.25),
    'Xbb_V3_ftop0': make_xbb_v3_getter(ftop=0),
    'Xbb_V3_ftop010': make_xbb_v3_getter(ftop=0.1),
    'Xbb_V3_ftop1': make_xbb_v3_getter(ftop=1.0),
    'Xbb_V3_ftop050': make_xbb_v3_getter(ftop=0.5),
    'Xbb_V3_ftop025': make_xbb_v3_getter(ftop=0.25),
    'mv2': make_mv2_getter(),
    'mv2_FR': make_mv2_getter(SJ_FR),
    'tau32_top_vs_qcd': get_tau32,
    'jss_top_vs_qcd': get_jsstop,
}
DISCRIMINANT_EDGES = {
    'dl1r': np.linspace(-10, 10, 1000),
    'dl1r_FR': np.linspace(-10, 10, 1000),
    'dl1r_VR': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop0': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop010': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop1': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop050': np.linspace(-10, 10, 1000),
    'Xbb_V1_ftop025': np.linspace(-10, 10, 1000),
    'Xbb_V2_ftop0': np.linspace(-10, 10, 1000),
    'Xbb_V2_ftop1': np.linspace(-10, 10, 1000),
    'Xbb_V2_ftop050': np.linspace(-10, 10, 1000),
    'Xbb_V2_ftop025': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop0': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop010': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop1': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop050': np.linspace(-10, 10, 1000),
    'Xbb_V3_ftop025': np.linspace(-10, 10, 1000),
    'mv2': np.linspace(-1, 1, 1000),
    'mv2_FR': np.linspace(-1, 1, 1000),
    'tau32_top_vs_qcd': np.linspace(0, 1, 1000),
    'jss_top_vs_qcd': np.linspace(0, 1, 1000),
}
