
DISCRIMINANT_NAME_MAP = {
    'dl1r': r'Min Ghost DL1r',
    'dl1r_FR': r'Min $R=0.2$ DL1r',
    'dl1r_VR': r'Min VR DL1r',
    'Xbb_V1_ftop0': r'$D^{}_{\rm Xbb}$, $f_{\rm top}=0$',
    'Xbb_V1_ftop010': r'$D^{}_{\rm Xbb}$, $f_{\rm top}=0.1$',
    'Xbb_V1_ftop1': r'$D^{}_{\rm Xbb}$, $f_{\rm top}=1$',
    'Xbb_V1_ftop050': r'$D^{}_{\rm Xbb}$, $f_{\rm top}=0.5$',
    'Xbb_V1_ftop025': r'$D^{}_{\rm Xbb}$, $f_{\rm top}=0.25$',
    'Xbb_V3_ftop0': r'$D^{V3}_{\rm Xbb}$, $f_{\rm top}=0$',
    'Xbb_V3_ftop010': r'$D^{V3}_{\rm Xbb}$, $f_{\rm top}=0.1$',
    'Xbb_V3_ftop1': r'$D^{V3}_{\rm Xbb}$, $f_{\rm top}=1$',
    'Xbb_V3_ftop050': r'$D^{V3}_{\rm Xbb}$, $f_{\rm top}=0.5$',
    'Xbb_V3_ftop025': r'$D^{V3}_{\rm Xbb}$, $f_{\rm top}=0.25$',
    'Xbb_V2_ftop025': r'$D^{V2}_{\rm Xbb}$, $f_{\rm top}=0.25$',
    'mv2': r'2 VR MV2',
    'mv2_FR': r'2 $R=0.2$ MV2',
    'tau32_top_vs_qcd': r'$\tau_{32}$',
    'jss_top_vs_qcd': r'DNN Top Tagger',
    'fatjet_eta': r'$\eta$',
    'fatjet_pt': r'$p_{\rm T}$ [TeV]',
    'fatjet_mass': r'$M_{\rm LRJ}$ [GeV]',
    'subjet_VRjet_dR': r'$\Delta R(subjet,VR-jet)$',
    'preselection': 'Preselection Jets'
}

DISCRIMINANT_NAME_MAP_SHORT = {
    'dl1r': r'minDL1rGhost',
    'dl1r_FR': r'minDL1rFR',
    'dl1r_VR': r'minDL1rVR',
    'mv2': 'MV2',
    'mv2_FR': 'MV2 FR',
    'Xbb_V1_ftop0': r'$Xbb^{f=0}_{V1}$',
    'Xbb_V1_ftop010': r'$Xbb^{f=0.1}_{V1}$',
    'Xbb_V1_ftop1': r'$Xbb^{f=1}_{V1}$',
    'Xbb_V1_ftop050': r'$Xbb^{f=0.5}_{V1}$',
    'Xbb_V1_ftop025': r'$Xbb^{f=0.25}_{}$',
    'Xbb_V3_ftop0': r'$Xbb^{f=0}_{V3}$',
    'Xbb_V3_ftop010': r'$Xbb^{f=0.1}_{V3}$',
    'Xbb_V3_ftop025': r'$Xbb^{f=0.25}_{V3}$',
    'Xbb_V3_ftop050': r'$Xbb^{f=0.5}_{V3}$',
    'Xbb_V3_ftop1': r'$Xbb^{f=1.0}_{V3}$',
    'Xbb_V2_ftop025': r'$Xbb^{f=0.25}_{V2}$',

}

DISCRIMINANT_COLOR_MAP = {
    'Xbb_V1_ftop0': 'lightskyblue',
    'Xbb_V1_ftop010': 'deepskyblue',
    'Xbb_V1_ftop025': 'darkred',
    'Xbb_V1_ftop050': 'blue',
    'Xbb_V1_ftop1': 'darkblue',
    'Xbb_V3_ftop0': 'lightskyblue',
    'Xbb_V3_ftop010': 'deepskyblue',
    'Xbb_V3_ftop025': 'darkred',
    'Xbb_V3_ftop050': 'blue',
    'Xbb_V3_ftop1': 'darkblue',
    'mv2': 'black',
    'dl1r': 'darkgreen',
    'preselection': 'black'

}

WP_COLOR_MAP = {
    '0.5':  'deepskyblue',
    '0.6':  'cornflowerblue',
    '0.7':  'royalblue',
    '0.8':  'darkblue',
}
FlAV_COLOR_MAP = {
    'all':  'black',
    '2b+0c': 'blue',
    '1b+1c': 'orange',
    '1b+0c': 'red',
    '2c+0b': 'green',
    '1c+0b': 'gray',
    '0b+0c': 'purple'
}
FlAV_NAME_MAP = {
    'all': 'All Flavours',
    '2b+0c': r'$\geq 2b + 0c$',
    '1b+1c': r'$1b + \geq 1c$',
    '1b+0c': r'$1b + 0c$',
    '2c+0b': r'$0b + \geq 2c$',
    '1c+0b': r'$0b + 1c$',
    '0b+0c': r'$0b + 0c$'
}
DISCRIMINANT_LS_MAP = {
    'dl1r': '-',
    'dl1r_FR': '-',
    'dl1r_VR': '-',
    'Xbb_V1_ftop0': '-',
    'Xbb_V1_ftop010': '-',
    'Xbb_V1_ftop1': '-',
    'Xbb_V1_ftop050': '-',
    'Xbb_V1_ftop025': '-',
    'Xbb_V3_ftop0': '-',
    'Xbb_V3_ftop010': '-',
    'Xbb_V3_ftop1': '-',
    'Xbb_V3_ftop050': '-',
    'Xbb_V3_ftop025': '-',
    'Xbb_V2_ftop025': '-',
    'mv2': '-',
    'mv2_FR': '-',
    'tau32_top_vs_qcd': '-',
    'jss_top_vs_qcd': '-',
    'fatjet_eta': '-',
    'fatjet_pt': '-',
    'fatjet_mass': '-',
    'subjet_VRjet_dR': '-',
    'preselection': '-',
}


PROCESS_NAME_MAP = {
    'dijet': 'Multijet',
    'top': 'Top',
    'higgs': 'Higgs'
}

# PROCESS_COLOR_MAP = {
#     'higgs': 'red',
#     'top': 'green',
#     'dijet': 'blue'
# }

# For discriminant plots
PROCESS_COLOR_MAP = {
    'higgs': '#ff7f0e',
    'top': '#2ca02c',
    'dijet': '#1f77b4'
}
