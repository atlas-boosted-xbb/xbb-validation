import os
import json
import re
import numpy as np
from h5py import File


def dhelp(info=None):
    """
    Help string formatter
    """
    if info:
        return f'{info}: default: %(default)s'
    return 'default: %(default)s'


def ap_default_args(def_val, info=None):
    """
    Default dictionary for argparse

    Returns args to set a default and help string.

    """
    return dict(default=def_val, help=dhelp(info), metavar='X')


def get_dsid(fpath):
    assert os.path.isdir(fpath), f'{fpath} is not a directory'
    if "data" in fpath.split('/')[-1]: return 0
    else: return int(os.path.basename(fpath).split('.')[2])


PROCESS_TO_JET_TYPE_MATCHERS = [
    ('dijet', 'QCD Pythia8 jets'),
    ('dijet_herwig', 'QCD Herwig7 jets'),
    ('data', 'Data jets'),
    ('higgs', 'Higgs-matched jets'),
    ('top', 'Top-matched jets'),
]

PRETTY_MATCHERS = [
    ('rsg_(?P<mgev>.*)', r'$m_{{G}} = {mgev}$ GeV'),
    ('zp_(?P<mgev>.*)', r"$m_{{Z'}} = {mgev}$ GeV"),
    ('wp_(?P<mgev>.*)', r"$m_{{W'}} = {mgev}$ GeV"),
    ('jz_(?P<jzslice>.*)', r'JZW slice {jzslice}'),
    ('jz_herwig_(?P<jzslice>.*)', r'H7 JZW slice {jzslice}'),
    ('data', r'data'),
]
TERSE_MATCHERS = [
    ('rsg_(?P<mgev>.*)', r'${mgev}$'),
    ('zp_(?P<mgev>.*)', r"${mgev}$"),
    ('wp_(?P<mgev>.*)', r"${mgev}$"),
    ('jz_(?P<jzslice>.*)', r'JZ{jzslice}W'),
    ('jz_herwig_(?P<jzslice>.*)', r'H7_JZ{jzslice}W'),
    ('data', r'data'),
]
UGLY_MATCHERS = [
    ('RS_G_hh_.*_M(?P<mgev>[0-9]+)' ,'rsg_{mgev}'  ),
    ('zprime(?P<mgev>[0-9]+)_tt'    ,'zp_{mgev}'   ),
    ('Wprime_WZqqqq_m(?P<mgev>[0-9]+)'    ,'wp_{mgev}'   ),
    ('jetjet_JZ(?P<jzslice>[0-9]+)W','jz_{jzslice}'),
    ('dipole_jetjetNLO_JZ(?P<jzslice>[0-9]+)WithSW', 'jz_herwig_{jzslice}'),
    ('data', 'data')
]
MATCHERS = {
    'ugly': UGLY_MATCHERS,
    'terse': TERSE_MATCHERS,
    'pretty': PRETTY_MATCHERS,
    'process': PROCESS_TO_JET_TYPE_MATCHERS
}


def get_name(fpath):
    assert os.path.isdir(fpath), f'{fpath} is not a directory'
    basename = os.path.basename(fpath)
    for exp, template in UGLY_MATCHERS:
        match = re.compile(exp).search(basename)
        if match:
            return template.format(**match.groupdict())


def get_formatted_name(name, matchers=TERSE_MATCHERS):
    try:
        matchers = MATCHERS[matchers]
    except TypeError:
        pass
    for exp, template in matchers:
        match = re.compile(exp).search(name)
        if match:
            return template.format(**match.groupdict())


def get_ds_weights_dict(json_file):
    return {int(k): v for k, v in json.load(json_file).items()}

def get_pt_eta_reweighting(jet_pt_eta_file,process, get_edges=False):

    with File(jet_pt_eta_file, 'r') as h5file:
        # Calculate the ratio of dijet histogram to PROC pT histogram
        num_pt = h5file['dijet']['pt_hist']
        denom_pt = h5file[process]['pt_hist']
        ratio_pt = np.zeros_like(num_pt)
        # don't divide by 0 array of bools, size = denom array
        valid = np.asarray(denom_pt) > 0.0
        # filter the ratio array with the valid array (this only returns ratio elements when denom is not zero)
        # otherwise, ratio is 0
        ratio_pt[valid] = num_pt[valid] / denom_pt[valid]
        # Calculate the ratio of dijet histogram to PROC eta histogram
        num_eta = h5file['dijet']['eta_hist']
        denom_eta = h5file[process]['eta_hist']
        ratio_eta = np.zeros_like(num_eta)
        # don't divide by 0 array of bools, size = denom array
        valid = np.asarray(denom_eta) > 0.0
        # filter the ratio array with the valid array (this only returns ratio elements when denom is not zero)
        # otherwise, ratio is 0
        ratio_eta[valid] = num_eta[valid] / denom_eta[valid]
        if(get_edges):
            pt_edges = h5file[process]['pt_edges']
            eta_edges = h5file[process]['eta_edges']
            return np.array(ratio_pt), np.array(ratio_eta), np.array(pt_edges), np.array(eta_edges)
        else:
            return ratio_pt, ratio_eta
