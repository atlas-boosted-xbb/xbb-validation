#!/usr/bin/env python3

"""
Make histograms of the jet pt spectra
"""

from argparse import ArgumentParser
from h5py import File
from glob import glob
import numpy as np
import json, os
from pathlib import PosixPath
import dask.array as da
from dask import compute

from xbb.common import filter_files_to_process_dict
from xbb.cross_section import CrossSections
from xbb.selectors import truth_match, EVENT_LABELS, all_events

def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('datasets', nargs='+', type=PosixPath)
    parser.add_argument('-d', '--denominator', required=False)
    parser.add_argument('-x', '--cross-sections', required=False)
    parser.add_argument('-o', '--out-dir', default='pt-hists', type=PosixPath)
    parser.add_argument('-g', '--print-graph', action='store_true')
    return parser.parse_args()

def array_from_dirs(dirs):
    arrs = []
    for d in dirs:
        h5ds = [File(f)['fat_jet'] for f in d.glob('*.h5')]
        dataset = da.concatenate([da.from_array(arr) for arr in h5ds])
        dataset['weight'] = dataset['mcEventWeight']
        arrs.append(dataset)
    return da.concatenate(arrs)

def run():
    edges = np.concatenate([[-np.inf], np.linspace(0, 3e6, 101), [np.inf]])
    args = get_args()
    processes = filter_files_to_process_dict(args.datasets)
    dijet = array_from_dirs(processes['dijet'])
    dijet_pt = da.histogram(dijet['pt'], edges)[0]
    dijet_counts = dijet.size
    args.out_dir.mkdir(exist_ok=True)
    if args.print_graph:
        dijet.visualize(str(args.out_dir / 'dijet-graph.svg'))
    outputs = {'pt': dijet_pt, 'counts': dijet_counts}
    res, = compute(outputs)
    print(res['pt'])
    print(f'{res["counts"]:,}')


if __name__ == '__main__':
    run()
