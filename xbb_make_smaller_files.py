#!/usr/bin/env python3

"""
Make slim datasets
"""

from argparse import ArgumentParser
from h5py import File
import numpy as np
import json, os
from pathlib import Path
from xbb_slim_dataset import define_variables

def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('dataset', type=Path)
    parser.add_argument('-o', '--out-dir', default='slimmed', type=Path)
    parser.add_argument('-f', '--force', action='store_true')
    parser.add_argument('-m', '--max_entries', default=100000)
    return parser.parse_args()

def build_smaller_file_for(ds, out_ds, N, is_data=False):
    md_key = 'metadata'
    #paths = ds.glob('all.h5')
    out_datasets = {}
    DATASETS_N_VARS = define_variables(is_data)
    with File(ds, 'r') as infile:
        for name, dsv in DATASETS_N_VARS.items():
            out_datasets[name] = OutputDataset(infile[name], out_ds, name, dsv)
        mdb = infile[md_key]
        metadata = OutputDataset(mdb, out_ds, md_key, mdb.dtype.names)
        for ds_name, ods in out_datasets.items():
            ods.make_shorter(infile[ds_name],N)
        metadata.add(infile[md_key])


class OutputDataset:
    def __init__(self, base_ds, out_group, output_name, variables):
        types = [(x, base_ds.dtype[x]) for x in variables]
        self.dataset = out_group.create_dataset(
            output_name, (0,), maxshape=(None,),
            dtype=types, chunks=(1000,),
            compression='gzip', compression_opts=7)
    def add(self, ds):
        oldmark = self.dataset.shape[0]
        self.dataset.resize(oldmark + ds.shape[0], axis=0)
        fields = self.dataset.dtype.names
        self.dataset[oldmark:] = np.asarray(ds[fields])
    def make_shorter(self, ds, N):
        #oldmark = self.dataset.shape[0]
        self.dataset.resize(N, axis=0)
        fields = self.dataset.dtype.names
        self.dataset[:] = np.asarray(ds[fields][0:N])

def run():
    args = get_args()
    output_ds = args.out_dir.joinpath(args.dataset.name)
    output_ds.mkdir(parents=True, exist_ok=True)
    with File(output_ds.joinpath('notall.h5'), 'w') as output:
        is_data = False if "data" not in str(args.dataset) else True
        build_smaller_file_for(output_ds.joinpath('all.h5'), output, args.max_entries, is_data = is_data)

if __name__ == '__main__':
    run()
