#!/usr/bin/env python3

"""

Produces plots of Kinematic Variable vs Efficiency per Variable bin
This means these are variable-efficiency plots, which are evaluated
for each variable's bin such that the signal efficiency in that bin
is at the requested WP.

It uses the output of make_2d_rocs.py which are the 2D histograms
with Discriminant vs Variable


"""

from argparse import ArgumentParser
from pathlib import Path
from collections import defaultdict

import numpy as np
from h5py import File
from matplotlib.ticker import MaxNLocator
from matplotlib.colors import to_rgba

from xbb.common import dhelp
from xbb.var_getters import DISCRIMINANT_GETTERS
from xbb.mpl import Canvas, helvetify, xlabdic, ylabdic, log_style
from xbb.mpl import add_kinematic_acceptance, add_atlas_label
from xbb.style import DISCRIMINANT_NAME_MAP, PROCESS_NAME_MAP
from xbb.style import DISCRIMINANT_COLOR_MAP, DISCRIMINANT_LS_MAP, WP_COLOR_MAP
# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_WP_HELP = 'Specify a WP to draw Sig Eff/ Bkg Rej vs Var plots for'
_VERBOSE_HELP = 'Turn on Verbose run'
_OUTDIR_HELP = dhelp('The directory where plots are saved')
_PT_RANGE_HELP = dhelp('The pT range to plot to apply to fat-jets')
_DISC_HELP = dhelp('The discriminants to plot curves for')
_VARS_HELP = dhelp('Kinematic Variable to plot against discriminant')
# ================
# Defaults for Args
# =================
WPs_DEFAULT = [50, 60, 70, 80]
DEFAULT_PT_RANGE = (250e3, np.inf)
DEFAULT_DISCRIMS = {'Xbb_V1_ftop025', 'Xbb_V3_ftop025', 'dl1r'}
DISC_CHOICES = set(DISCRIMINANT_GETTERS.keys())
DEFAULT_VAR = 'fatjet_pt'
VAR_CHOICES = {'fatjet_eta', 'fatjet_pt', 'fatjet_mass'}


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('roc_2d_file', type=Path)
    parser.add_argument('--var', choices = VAR_CHOICES, required=True, help =_VARS_HELP)
    parser.add_argument('-r', '--roc-discriminants', nargs='+', choices=DISC_CHOICES,
                        default=DEFAULT_DISCRIMS, help=_DISC_HELP)
    parser.add_argument('-wp', '--working-points', nargs='+', default=WPs_DEFAULT,
                        type=int, help=_WP_HELP)
    parser.add_argument('-o', '--out-dir', type=Path, default='plots/varying_cut_eff',
                        help=_OUTDIR_HELP)
    parser.add_argument('-e', '--errorbar', action='store_true')
    parser.add_argument('-a', '--approved', action='store_true')
    parser.add_argument('-l', '--log', action='store_true')

    return parser.parse_args()


# ==================================================================
# main()
# ==================================================================
def run():

    args = get_args()
    hist2d_file = args.roc_2d_file
    var_to_plot = args.var
    discrims_asked = args.roc_discriminants  # Discriminants to Study
    make_log = args.log
    working_pts = args.working_points
    plot_errbar = args.errorbar
    is_plot_atlas_approved = args.approved
    # ================
    # Prepare Output directory
    # ================
    # Get out-dir
    outdir = args.out_dir
    outdir.mkdir(parents=True, exist_ok=True)

    # ================
    # Run Methods
    # ================
    (pt_range_to_discrims_to_proc_to_histos_map,
        pt_range_to_discrims_to_var_edges_map) = prepare_input_data(hist2d_file, var_to_plot)
    process_eff_plots(pt_range_to_discrims_to_proc_to_histos_map,
                      pt_range_to_discrims_to_var_edges_map,
                      var_to_plot, working_pts, discrims_asked, outdir,
                      signal='higgs', log_eff_axis=make_log,
                      errorbar=plot_errbar, approved=is_plot_atlas_approved)


# ==================================================================
# Get the data from input file ready
# this function depends on the structure of input file and nesting
# ==================================================================
def prepare_input_data(input_file, var_to_plot):

    # ================================
    # Open input file and get the 2D Histos of Var vs Discriminant
    # ================================
    with File(input_file, 'r') as histos_2d:
        # Mapping each pT range to its data
        pt_range_to_discrims_to_proc_to_histos_map = {}
        pt_range_to_discrims_to_var_edges_map = {}
        # Loop over the pT ranges produced by make_2d_rocs.[y]
        for pt_range_name, pt_range_group in histos_2d.items():
            # Map from discriminant to process to histos for each pT range
            discrim_to_proc_to_histo_map = {}
            discrim_to_var_edges_map = {}
            # Loop over discriminants in files
            for discrim_name, discrim_group in pt_range_group.items():
                # Make a map discrim -> process -> prcoess_2D_histogram
                proc_name_to_histo_map = {proc_name:
                                          (np.asarray(discrim_group[proc_name]['hist_wt']),
                                           np.asarray(discrim_group[proc_name]['hist_wt2']))
                                          for proc_name in discrim_group.keys()}
                # Array to hold the arr [(var_min, var_max), (discrim_min, discrim_max)]
                limits_arr = [discrim_group[proc_name].attrs['limits']
                              for proc_name in discrim_group.keys()]
                # Make sure that all limits are the same for the different processes
                assert all((x == limits_arr[0]).all() for x in limits_arr)
                # Declare map process -> rebinned histo for each discriminant
                proc_name_to_rebinned_histo_map = {}
                # Loop over processes
                for process_name, histos in proc_name_to_histo_map.items():
                    # Declare iterable to store rebinned hist_wt and hist_wt2
                    rebinned_histos = []
                    # Loop over hist_wt and hist_wt2 and rebin tha kinematic variable axis
                    for histo in histos:
                        # Use just one of the process limits since they're all the same per discrim
                        (rebinned_hist, 
                            rebinned_var_edges) = get_rebinned(histo, limits_arr[0], var_to_plot)
                        # Store the rebinned histograms
                        rebinned_histos.append(rebinned_hist)
                    # Fill the process -> (hist_wt, hist_wt2) map
                    proc_name_to_rebinned_histo_map[process_name] = (rebinned_histos[0],
                                                                     rebinned_histos[1])
                # Map discriminant to its (proc, (hist_wt, hist_wt2)) map
                discrim_to_proc_to_histo_map[discrim_name] = proc_name_to_rebinned_histo_map
                # Variable edges are the same for both hist_wt and hist_wt2
                discrim_to_var_edges_map[discrim_name] = rebinned_var_edges
            # Fill the map from pT range to discrim to process to process histos
            pt_range_to_discrims_to_proc_to_histos_map[pt_range_name] = discrim_to_proc_to_histo_map
            # Fill the map from pT range to discrim to variable edges
            pt_range_to_discrims_to_var_edges_map[pt_range_name] = discrim_to_var_edges_map

    return pt_range_to_discrims_to_proc_to_histos_map, pt_range_to_discrims_to_var_edges_map


# ==================================================================
# This Rebins the variable axis, in case its too fine!
# ==================================================================
VAR_BINSIZE_MAP = {
    'fatjet_pt': 125e3,
    'fatjet_eta': 0.2,
    'fatjet_mass': 5e3,
}


def get_rebinned(hist, limits, var_to_plot):
    var_bounds = limits[1]
    # -2 for overflow, +1 for fenceposting
    num_var_edges_old = hist.shape[1] - 1

    # =================
    # Aim for 1 bin every X Units
    # We want all new edges to be members of old edges list
    # this way all data will nicely fall in a bin without assumption
    # on where the data is inside an old bin.
    # We try a range of binsizes, up to 10% of size asked
    # =================

    # Get the old and new edges
    old_edges = np.linspace(*var_bounds, num_var_edges_old)
    new_edges = get_best_new_edges(VAR_BINSIZE_MAP[var_to_plot], old_edges, var_bounds, delta=0.1)

    # =================
    # Make the new rebinned histogram
    # =================
    # Use digitize to find which New bin does old edge fall in
    # digitize numbers the bins starting from bin "1"
    # Size of bin_map = size of old edges
    bin_map = np.concatenate([[0], np.digitize(old_edges, new_edges)])

    old_bins_sums = []
    # Turn bin_map to just a set of bin numbers then loop over each new bin
    for bin_num in sorted(set(bin_map)):
        #  bin by bin from the new ones, we sum up all old bins that fall inside it
        #  Filter out indices of bin_map with this bin number
        #  return the bin_map indicies where an old bin fell in bin_num
        #  If first 2 old bins are in bin_num return [0,1], etc..
        old_bins_in_this_bin = np.where(bin_map == bin_num)[0]
        # Slice the histo by selecting y-axis bins e.g. 1,2 and summing them up
        sum_slice = hist[:, old_bins_in_this_bin].sum(axis=1)
        # append the sum of bins in each slice
        old_bins_sums.append(sum_slice)
    # return all sums for all slices (shape discrim.size, new_pT.size, and the new bounds
    return np.stack(old_bins_sums, axis=1), np.array(new_edges)


# ==================================================================
# Function takes in a bin-size to rebin to, and if all the new bin-edges
# do not fall "close enough" to old bin-edges, it surveys bin sizes up to
# X% larger than requested to find best bin-size then use it.
# X is specfied by the arg freedom.
# ==================================================================
def get_best_new_edges(bin_size_needed, old_edges, var_bounds, delta=0.1):
    # Make a list of acceptable bin-sizes
    bin_sizes_range = np.linspace(bin_size_needed, (1+delta)*bin_size_needed, 100)
    # Prepare a bool to flag if a binsize was found where
    # all new bin-edges are close to old-edges
    bin_size_found = False
    # Prepare array to hold new edges
    chosen_new_edges = old_edges

    # Loop over binsizes
    for bin_size in bin_sizes_range:
        # Get the number of bin edges corressponding to the bin-size
        num_var_edges_new = int((var_bounds[1] - var_bounds[0]) // bin_size) + 1
        # Get the new edges
        new_edges = np.linspace(*var_bounds, num_var_edges_new)

        # Array to hold bools checking if each candidate
        # new edge is close enough to any old edge
        new_edge_near_an_old_edge = []
        # Loop over candidate new-edges
        for new_edge in new_edges:
            # Subtract away new edge from all old-edges
            old_minus_new = np.absolute(np.array(old_edges)-new_edge)
            # Check if smallest difference is < 0.01
            if(old_minus_new.min() < 0.01):
                # edge is definitly near-enough an old edge
                new_edge_near_an_old_edge.append(True)
            else:
                new_edge_near_an_old_edge.append(False)
        # Check if all new edges were found close to old-edges
        if(not all(near_edge for near_edge in new_edge_near_an_old_edge)):
            continue
        else:
            # If all new-edges were nere old-edges, we're done!
            return new_edges

    # If we come here, no new-edgees were found. Break code.
    assert False

    return chosen_new_edges


# ==================================================================
# Function responsible to process the data saved from input file
# and run a masking process in the 2D discrim-var plane for
# bins with efficiency < WP requested
# and then call a drawing function to plot and save the rocs
# ==================================================================
def process_eff_plots(pt_range_to_discrims_to_proc_to_histos_map,
                      pt_range_to_discrims_to_var_edges_map, var_to_plot,
                      WPs, discrims_asked, outdir,
                      signal='higgs', **plot_args):
    # ================================
    # Masking signal 2D bins with SigEff < WP
    # and getting efficiency 2D histograms
    # ================================

    for pt_range_name, discrim_to_proc_to_histo_map in pt_range_to_discrims_to_proc_to_histos_map.items():
        # Declare a map from discriminant to efficiency histogram with a given Mask
        wp_discrim_to_proc_to_eff_data_dict = {}
        for discrim_name, proc_name_to_histo_map in discrim_to_proc_to_histo_map.items():
            if discrim_name not in discrims_asked:
                continue
            var_edges = pt_range_to_discrims_to_var_edges_map[pt_range_name][discrim_name]
            # Get the masking using signal process to be applied to backgrounds
            for sig_eff in WPs:
                sig_eff = sig_eff/100
                # Get the mask which wills select discrim bins per Var bin where eff < sigeff
                mask = get_mask(proc_name_to_histo_map[signal][0], sig_eff=sig_eff)
                # Initalise a dictionary for each signal efficiency requested
                wp_discrim_to_proc_to_eff_data_dict[(str(sig_eff), discrim_name)] = {}
                for proc_name, (hist_wt, hist_wt2) in proc_name_to_histo_map.items():
                    # Get the 2D Histo of efficiencies, the corresponding error histo
                    # and a mask to remove var-bins that fail the WP completely
                    eff_hist, eff_error, var_mask = get_efficiency(hist_wt, hist_wt2, mask)
                    # Build a dictionary with all info for the eff vs variable plot!
                    wp_discrim_to_proc_to_eff_data_dict[(str(sig_eff), discrim_name)][proc_name] = dict(eff_hist=eff_hist, wp=sig_eff, var_mask=var_mask,
                                                                                                        var_edges=var_edges, discrim=discrim_name,
                                                                                                        eff_error=eff_error)

        # ================================
        # We need to get the info we have in a format
        # allowing to overlay different discrims
        # per WP per process
        # ================================
        # Loop over eff requested
        for sig_eff in WPs:
            sig_eff = sig_eff/100
            # Get all the process->eff_data maps for this signal eff (from all discriminants)
            proc_to_eff_data_dict_list = [value
                                          for keys, value in wp_discrim_to_proc_to_eff_data_dict.items()
                                          if keys[0] == str(sig_eff)]
            # Map process to a list of efficiency data for all discriminants
            proc_to_discrims_eff_map = defaultdict(list)
            # Loop over the process-> eff_data for each discriminant for this sig eff
            for proc_to_eff_data_dict in proc_to_eff_data_dict_list:
                # Loop over the processes
                for proc_name, eff_data_dict in proc_to_eff_data_dict.items():
                    # append eff_data for this process from each discrim
                    proc_to_discrims_eff_map[proc_name].append(eff_data_dict)
            # Now loop over the processes, and pass the eff data for all discrims to plot
            for proc_name, all_discrims_eff_data in proc_to_discrims_eff_map.items():
                draw_plot(all_discrims_eff_data, outdir, proc_name, 
                          var_to_plot, sig_eff_cut=sig_eff, signal=signal,**plot_args)
        # ================================
        # We need to get the info we have in a format
        # allowing to overlay different working points
        # per discrim per process
        # ================================
        for discrim_name in discrims_asked:
            # Get all the process->eff_data maps for this discrim (from all WPs)
            proc_to_eff_data_dict_list = [value for keys, value in wp_discrim_to_proc_to_eff_data_dict.items() if keys[1] == discrim_name]
            # Map process to a list of efficiency data for all WPs per discriminant
            proc_to_wps_eff_map = defaultdict(list)
            # Loop over the process-> eff_data for each discriminant for this sig eff
            for proc_to_eff_data_dict in proc_to_eff_data_dict_list:
                # Loop over the processes
                for  proc_name, eff_data_dict in proc_to_eff_data_dict.items():
                    # append eff_data for this process from each discrim
                    proc_to_wps_eff_map[proc_name].append(eff_data_dict)

            # Now loop over the processes, and pass the eff data for all WPs to plot
            for proc_name, all_wps_eff_data in proc_to_wps_eff_map.items():
                draw_plot(all_wps_eff_data, outdir, proc_name, var_to_plot, overlay_wps=True, signal=signal,**plot_args)


# ==================================================================
# Function returns an arr of bools for each var-bin, where entries are False
# for disrim bins which when summed give eff > sigeff, and True for the rest
# for one var-bin, if 4 discrim-bins have entries [5,15,20,30]
# then for 70% WP we want to keep only [20,30] return is [False, False, True, True]
# ==================================================================
def get_mask(hist, sig_eff):

    # Start with [var[1][1], var[1][2], var[1][3]....]_disc[1] , [var[2][1], var[2][2], var[2][3]....]_disc[2],...
    # Integerate histo along the discriminant axis (with N bins) in reverse order
    # this gives sum of entries in each var bin for all discrim bins
    # e.g. [var[N][1]+..+var[2][1]+var[1][1], var[N][2]+..+var[1][2], ...]_disc[1], [var[N][1]+..+var[2][1], ....]_disc[2],..]
    disc_axis_integrated = hist[::-1,:].cumsum(axis=0)[::-1,:]

    # Check for any empty var bins
    # The 0th element of disc_axis_integrated has the sum of all discrim bins for each var bin
    var_bin_has_zero_entries = (disc_axis_integrated[0,:] == 0)
    # Get the efficiency in each variable bin
    efficiency_per_var_bin = disc_axis_integrated # wait for it... its just a rename
    efficiency_per_var_bin[:,~var_bin_has_zero_entries] /= efficiency_per_var_bin[0,~var_bin_has_zero_entries]
    # Check for efficiencies smaller than requested WP
    put_mask = (efficiency_per_var_bin < sig_eff)
    # we might as well mask off the var bins with zero entries
    put_mask[:, var_bin_has_zero_entries] = False
    # Return the mask
    return put_mask


# ==================================================================
# Function responsible to calculate efficiency for all processes
# along with error calculation. Takes in the 2D
# discrim-var histo and its cousin with weights^2
# ==================================================================
def get_efficiency(hist_wt, hist_wt2, mask):

    # Get the sum of entries in each variable bin
    total = hist_wt.sum(axis=0)
    total_err_sq = hist_wt2.sum(axis=0)
    # Get the sum of entries in each variable bin excluding masked bins
    tagged = hist_wt.sum(axis=0, where=mask)
    tagged_err_sq = hist_wt2.sum(axis=0, where=mask)
    # Get the number of untagged entries in each variable bin
    untagged = total - tagged
    untagged_err_sq = total_err_sq - total_err_sq

    # ================================
    # We want to keep only the variable bins where
    # there are some discrim bins with untagged jets
    # if zero discrim bins have untagged jets, it means
    # all jets in the var-bin has failed to pass the WP
    # or there are no jets in that var-bin.
    # ================================
    # We get the number of discriminant bins
    num_discrim_bins = mask.shape[0]
    # We get the number of discrim bins per var-bin
    # where there are tagged jets
    num_discrim_bins_tagged = mask.sum(axis=0)
    # Subtract the two, to get number of discrim bins
    # per var-bin with untagged jets
    # If we have 1 discrim bin with untagged jets in a var-bin, use the var_bin
    var_mask = num_discrim_bins - num_discrim_bins_tagged > 1

    # We define variable bins with no entries to have NaN eff
    eff = np.full_like(total, np.nan)
    eff_error = np.full_like(total, np.nan)
    unempty_bins = total > 0

    # Efficiency
    eff[unempty_bins] = tagged[unempty_bins] / total[unempty_bins]
    # Use binomial error (see line 3026 https://root.cern.ch/doc/master/TH1_8cxx_source.html)
    eff_error = np.sqrt(abs( ( (1. - 2.* eff) * tagged_err_sq  + (eff**2) * (total_err_sq) ) / total**2 ) )

    return eff, eff_error, var_mask


# ==================================================================
# This is responsible for drawing the Canvas for one process
# here overlay either different discrims or different WPs per discrim
# ==================================================================
# hand-optimized efficiency ranges (delete to get auto range)
SAMPLE_RANGES_LOG = {
    ('dijet', 60): (4, 800),
    ('top'  , 60): (2, 200),
    ('higgs', 60): (1, 5),
}
SAMPLE_RANGES = {
    ('dijet', 50): (1, 320),
    ('top',   50): (1, 200),
    ('dijet', 60): (1, 220),
    ('top',   60): (1, 75),
    ('dijet', 70): (1, 140),
    ('top',   70): (1, 35),
}


def draw_plot(all_overlaid_eff_data, outdir, proc_name,
              var_to_plot,
              sig_eff_cut = None,
              overlay_wps=False,
              signal = 'higgs',
              log_eff_axis=False,
              errorbar=False, approved=False):

    # ================================
    # Prepare output file
    # ================================
    outfile = f'{proc_name}_LogScale.pdf' if log_eff_axis else f'{proc_name}.pdf'
    eff_in_percent = 0
    if(not overlay_wps):
        eff_in_percent = float(sig_eff_cut * 100)
        eff_name = f'const_eff_{eff_in_percent:.0f}'
        outpath = outdir/'overlay_discrims'/eff_name
        outpath.mkdir(exist_ok=True, parents=True)
    else:
        outfile = f'{proc_name}_LogScale.pdf' if log_eff_axis else f'{proc_name}.pdf'
        discrim = all_overlaid_eff_data[0]['discrim']
        outpath=outdir/'overlay_wps'/discrim
    # Open a Canvas
    with Canvas(outpath/outfile, fontsize=24) as can:
        # ================================
        # Prepare input data
        # ================================
        # Loop over the efficiency info for the different lines to be overlaid
        for eff_data in all_overlaid_eff_data:
            # Name of the discriminant
            discrim_name = eff_data['discrim']
            # The working point selected
            wp = eff_data['wp']
            # The efficiency vs variable histo
            eff_hist = eff_data['eff_hist']
            # The variable axis edges
            var_edges = eff_data['var_edges']
            var_mask = eff_data['var_mask'][1:-1] # Ignore overflow and underflow bins eff
            # Get units to TeV[GeV] if pT[Mass] is plotted
            if(var_to_plot=='fatjet_pt'):   var_edges = var_edges * 1e-6
            if(var_to_plot=='fatjet_mass'):   var_edges = var_edges * 1e-3
            # Get the variable axis centers
            var_centers = (var_edges[:-1] + var_edges[1:]) / 2
            var_centers, eff_hist = var_centers[var_mask], eff_hist[1:-1][var_mask]
            # The error on the variable (horizontal error)
            var_err = np.stack([var_centers - var_edges[:-1][var_mask], var_edges[1:][var_mask] - var_centers]) # What?
            # The error on the efficiency
            eff_error = eff_data['eff_error'][1:-1][var_mask]
            # ================================
            # Prepare some line styling
            # ================================
            # in case we are overlaying Discriminants
            nice_discrim_name = DISCRIMINANT_NAME_MAP[discrim_name]
            discrim_color = DISCRIMINANT_COLOR_MAP[discrim_name]
            discrim_linestyle = DISCRIMINANT_LS_MAP[discrim_name]
            # in case we are overlaying WPs
            wp_color = WP_COLOR_MAP[str(wp)]
            # Get an error plotting function ready
            plot = draw_errorbar if errorbar else draw_fill_between
            # Make a plot with errors
            plot(can.ax, proc_name = proc_name, signal = signal,
                var_centers=var_centers, eff_hist=eff_hist, eff_error=eff_error,
                discrim_color=discrim_color, discrim_linestyle=discrim_linestyle,
                discrim_name=nice_discrim_name, var_err=var_err, wp = wp,
                wp_color = wp_color, overlay_wps = overlay_wps)
        # ================================
        # Prepare some canvas styling
        # ================================
        # X and Y labels, ticks and scales
        # ================
        process_name = PROCESS_NAME_MAP[proc_name]
        if(proc_name==signal):
            y_label = f'{process_name} Efficiency'
        else:
            y_label = f'{process_name} Rejection'
        can.ax.set_ylabel(y_label, **ylabdic(28))
        var_name = DISCRIMINANT_NAME_MAP[var_to_plot]
        can.ax.set_xlabel(fr'Large-$R$ Jet {var_name}',**xlabdic(28))
        can.ax.yaxis.set_major_locator(MaxNLocator(prune='upper'))
        # Scale
        if log_eff_axis:    log_style(can.ax)
        hard_coded_eff_range = SAMPLE_RANGES_LOG if log_eff_axis else SAMPLE_RANGES
        if(not overlay_wps):
            if (proc_name, int(eff_in_percent)) in hard_coded_eff_range:
                can.ax.set_ylim(*SAMPLE_RANGES[proc_name, eff_in_percent])
        # ================
        # X bounds
        # ================
        # The X limits will depend on presence of error bar extending left/right
        lower_bound = var_edges[0] if errorbar else var_centers[0]
        upper_bound = (var_edges[1:] if errorbar else var_centers).max()
        can.ax.set_xlim(lower_bound, upper_bound)
        # ================
        # Adding Text info
        # ================
        can.ax.legend(loc = 'upper right', frameon=False, fontsize=16)
        # If we are plotting eff v mass, change mass cut text
        mass_in_presel = True
        if(var_to_plot=='fatjet_mass'):    mass_in_presel = False
        if(not overlay_wps):
            add_kinematic_acceptance(can.ax, eff=eff_in_percent, offset=0.05, mass_in_presel=mass_in_presel)
        else:
            add_kinematic_acceptance(can.ax, offset=0.05, mass_in_presel=mass_in_presel)
        add_atlas_label(can.ax, vshift=0.05, internal=(not approved))


# ==================================================================
# This is responsible for making var vs eff plot with shaded region instead of error bar
# ==================================================================
def draw_fill_between(ax, proc_name, signal, var_centers, eff_hist, eff_error,
                      discrim_color, discrim_linestyle, discrim_name, wp,
                      overlay_wps, wp_color, **ignore):
    # The y-axis is eff for signal, 1/eff for bkg samples
    if(proc_name == signal):
        yaxis = eff_hist
        errorbars = eff_error
    else:
        yaxis = 1/eff_hist
        rej_err = abs(eff_error/eff_hist**2)
        errorbars = rej_err

    # Line label
    label = f'{discrim_name} at {wp*100:.0f}%' if overlay_wps else discrim_name
    # Line color
    line_color = wp_color if overlay_wps else discrim_color
    # Make the plot
    ax.plot(var_centers, yaxis,
            label=label, color=line_color, linestyle=discrim_linestyle,
            linewidth=3)
    # Fill area around line corresponding to error
    ax.fill_between(var_centers,
                    (yaxis-errorbars),
                    (yaxis+errorbars),
                    color=line_color, alpha=0.1)


# ==================================================================
# This is responsible for making var vs eff plot with x and y error bars
# ==================================================================
def draw_errorbar(ax, proc_name, signal, var_centers, eff_hist, eff_error,
                  discrim_color, discrim_linestyle, discrim_name, var_err,
                  wp, overlay_wps, wp_color):
    # The y-axis is eff for signal, 1/eff for bkg samples
    if(proc_name == signal):
        yaxis = eff_hist
        errorbars = eff_error
    else:
        yaxis = 1/eff_hist
        rej_err = abs(eff_error/eff_hist**2)
        errorbars = rej_err


    # ================================
    # Styling for markers
    # ================================
    line_color = wp_color if overlay_wps else discrim_color

    light_color = to_rgba(line_color, 0.6)
    bar_color = line_color
    markerfacecolor = None
    markeredgecolor = line_color
    marker = 'o'
    markeredgewidth = 1
    linewidth = 4

    if discrim_linestyle == '--':
        linewidth = 2
        markeredgewidth = 2
        markerfacecolor = 'none'
        marker = 'o'
        bar_color = line_color
    # Line label
    label = f'{discrim_name} at {wp*100:.0f}% WP' if overlay_wps else discrim_name

    # Make the plot
    ax.errorbar(var_centers, yaxis, errorbars, var_err,
                marker=marker, markersize=10,
                label=label, color=bar_color,
                linestyle='none', linewidth=linewidth,
                markerfacecolor=markerfacecolor,
                markeredgecolor=markeredgecolor,
                markeredgewidth=markeredgewidth)


if __name__ == '__main__':
    run()
