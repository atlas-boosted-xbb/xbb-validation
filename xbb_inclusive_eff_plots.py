#!/usr/bin/env python3

"""

This script produces kinematic variable vs efficiency where efficiency
is calculated by using a fixed cut on the discriminant histogram

It uses the output of make_2d_rocs.py which are the 2D histograms
with Discriminant vs Variable


"""

from argparse import ArgumentParser
from pathlib import Path
from collections import defaultdict

import numpy as np
import h5py
from h5py import File
from matplotlib.ticker import MaxNLocator
from matplotlib.colors import to_rgba

from xbb.common import dhelp
from xbb.var_getters import DISCRIMINANT_GETTERS
from xbb.mpl import Canvas, CanvasWithRatio, helvetify, xlabdic, ylabdic, log_style
from xbb.mpl import add_kinematic_acceptance, add_atlas_label
from xbb.style import DISCRIMINANT_NAME_MAP, PROCESS_NAME_MAP
from xbb.style import DISCRIMINANT_COLOR_MAP, DISCRIMINANT_LS_MAP, WP_COLOR_MAP
# ==================================================================
# Get arguments from CL
# ==================================================================
# Help messages
# ================
_WP_HELP = 'Specify a WP to draw Sig Eff/ Bkg Rej vs Var plots for'
_VERBOSE_HELP = 'Turn on Verbose run'
_OUTDIR_HELP = dhelp('The directory where plots are saved')
_PT_RANGE_HELP = dhelp('The pT range to plot to apply to fat-jets')
_DISC_HELP = dhelp('The discriminants to plot curves for')
_VARS_HELP = dhelp('Kinematic Variable to plot against discriminant')
_OVERLAY_WPS_HELP = 'Overlay the different WPs for each discriminant'
# ================
# Defaults for Args
# =================
WPs_DEFAULT = [50, 60, 70, 80]
DEFAULT_PT_RANGE = (250e3, np.inf)
DEFAULT_DISCRIMS = {'Xbb_V1_ftop025', 'Xbb_V3_ftop025', 'dl1r'}
DISC_CHOICES = set(DISCRIMINANT_GETTERS.keys())
DEFAULT_VAR = 'fatjet_pt'
VAR_CHOICES = {'fatjet_eta', 'fatjet_pt', 'fatjet_mass'}


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('roc_2d_file', type=Path)
    parser.add_argument('--var', choices=VAR_CHOICES, required=True, help=_VARS_HELP)
    parser.add_argument('-r', '--roc-discriminants', nargs='+', choices=DISC_CHOICES,
                        default=DEFAULT_DISCRIMS, help=_DISC_HELP)
    parser.add_argument('-wp', '--working-points', nargs='+', default=WPs_DEFAULT, type=int,
                        help=_WP_HELP)
    parser.add_argument('-cwp', '--compare-wps', action='store_true', help=_OVERLAY_WPS_HELP)
    parser.add_argument('-o', '--out-dir', type=Path, default='plots/varying_cut_eff',
                        help=_OUTDIR_HELP)
    parser.add_argument('-e', '--errorbar', action='store_true')
    parser.add_argument('-a', '--approved', action='store_true')
    parser.add_argument('-l', '--log', action='store_true')

    return parser.parse_args()


# ==================================================================
# main()
# ==================================================================
def run():
    args = get_args()
    hist2d_file = args.roc_2d_file
    var_to_plot = args.var
    discrims_asked = args.roc_discriminants  # Discriminants to Study
    make_log = args.log
    working_pts = args.working_points
    plot_errbar = args.errorbar
    is_plot_atlas_approved = args.approved
    # ================
    # Prepare Output directory
    # ================
    # Get out-dir
    outdir = args.out_dir
    outdir.mkdir(parents=True, exist_ok=True)

    # ================
    # Run Methods
    # ================
    (pt_range_to_discrims_to_proc_to_histos_map,
        pt_range_to_discrims_to_edges_map) = prepare_input_data(hist2d_file, var_to_plot)

    process_eff_plots(pt_range_to_discrims_to_proc_to_histos_map, pt_range_to_discrims_to_edges_map,
                      var_to_plot, working_pts, discrims_asked, outdir,
                      signal='higgs', approved=is_plot_atlas_approved)


# ==================================================================
# Get the data from input file ready
# this function depends on the structure of input file and nesting
# ==================================================================
def prepare_input_data(input_file, var_to_plot):

    # ================================
    # Open input file and get the 2D Histos of Var vs Discriminant
    # ================================
    with File(input_file, 'r') as histos_2d:
        # Mapping each pT range to its data
        pt_range_to_discrims_to_proc_to_histos_map = {}
        pt_range_to_discrims_to_edges_map = {}
        # Loop over the pT ranges produced by make_2d_rocs.[y]
        for pt_range_name, pt_range_group in histos_2d.items():
            # Map from discriminant to process to histos for each pT range
            discrim_to_proc_to_histo_map = {}
            discrim_to_edges_map = {}
            # Loop over discriminants in files
            for discrim_name, discrim_group in pt_range_group.items():
                # Make a map discrim -> process -> prcoess_2D_histogram
                proc_name_to_histo_map = {proc_name:
                                          (np.asarray(discrim_group[proc_name]['hist_wt']),
                                           np.asarray(discrim_group[proc_name]['hist_wt2']))
                                          for proc_name in discrim_group.keys()}
                # Array to hold the arr [(var_min,var_max), (discrim_min,discrim_max)]
                limits_arr = [discrim_group[proc_name].attrs['limits']
                              for proc_name in discrim_group.keys()]
                # Make sure that all limits are the same for the different processes
                assert all((x == limits_arr[0]).all() for x in limits_arr)
                # Declare map process -> rebinned histo for each discriminant
                proc_name_to_rebinned_histo_map = {}
                # Loop over processes
                for process_name, histos in proc_name_to_histo_map.items():
                    # Declare iterable to store rebinned hist_wt and hist_wt2
                    rebinned_histos = []
                    # Loop over hist_wt and hist_wt2 and rebin tha kinematic variable axis
                    for histo in histos:
                        # Use just one of the process limits since they're all the same per discrim
                        (rebinned_hist,
                            rebinned_var_edges) = get_rebinned(histo, limits_arr[0], var_to_plot)
                        # Variable edges are the same for both hist_wt and hist_wt2
                        # Store the rebinned histograms
                        rebinned_histos.append(rebinned_hist)
                        # Get discrim edges -- same for hist_wt and and hist_wt2
                        num_discirim_edges = histo.shape[0] + 1
                        discrim_edges = np.linspace(*limits_arr[0][0], num_discirim_edges)
                    # Fill the process -> (hist_wt,hist_wt2) map
                    proc_name_to_rebinned_histo_map[process_name] = (rebinned_histos[0],
                                                                     rebinned_histos[1])
                # Map discriminant to its (proc, (hist_wt,hist_wt2)) map
                discrim_to_proc_to_histo_map[discrim_name] = proc_name_to_rebinned_histo_map
                discrim_to_edges_map[discrim_name] = (rebinned_var_edges, discrim_edges)

            # Fill the map from pT range to discrim to process to process histos
            pt_range_to_discrims_to_proc_to_histos_map[pt_range_name] = discrim_to_proc_to_histo_map
            # Fill the map from pT range to discrim to variable edges
            pt_range_to_discrims_to_edges_map[pt_range_name] = discrim_to_edges_map

    return pt_range_to_discrims_to_proc_to_histos_map, pt_range_to_discrims_to_edges_map


# ==================================================================
# This Rebins the variable axis, in case its too fine!
# ==================================================================
VAR_BINSIZE_MAP = {
    'fatjet_pt': 125e3,
    'fatjet_eta': 0.2,
    'fatjet_mass': 5e3,
}


def get_rebinned(histo, limits, var_to_plot):

    var_bounds = limits[1]
    # +1 for fenceposting
    num_var_edges_old = histo.shape[1] - 1

    # =================
    # Aim for 1 bin every X Units
    # We want all new edges to be members of old edges list
    # this way all data will nicely fall in a bin without assumption
    # on where the data is inside an old bin.
    # We try a range of binsizes, up to 10% of size asked
    # =================

    # Get the old and new edges
    old_edges = np.linspace(*var_bounds, num_var_edges_old)
    new_edges = get_best_new_edges(VAR_BINSIZE_MAP[var_to_plot], old_edges, var_bounds, delta=0.1)

    # =================
    # Make the new rebinned histogram
    # =================
    # Use digitize to find which New bin does old edge fall in
    # digitize numbers the bins starting from bin "1"
    # Size of bin_map = size of old edges
    bin_map = np.concatenate([[0], np.digitize(old_edges, new_edges)])

    old_bins_sums = []
    # Turn bin_map to just a set of bin numbers then loop over each new bin
    for bin_num in sorted(set(bin_map)):
        #  bin by bin from the new ones, we sum up all old bins that fall inside it
        #  Filter out indices of bin_map with this bin number
        #  return the bin_map indicies where an old bin fell in bin_num
        #  If first 2 old bins are in bin_num return [0,1], etc.
        old_bins_in_this_bin = np.where(bin_map == bin_num)[0]
        # Slice the histo by selecting variable bins e.g. 1,2 and summing them up
        sum_slice = histo[:, old_bins_in_this_bin].sum(axis=1)
        # append the sum of bins in each slice
        old_bins_sums.append(sum_slice)
    # return all sums for all slices (shape discrim.size, new_pT.size, and the new bounds
    return np.stack(old_bins_sums, axis=1), np.array(new_edges)


# ==================================================================
# Function takes in a bin-size to rebin to, and if all the new bin-edges
# do not fall "close enough" to old bin-edges, it surveys bin sizes up to
# X% larger than requested to find best bin-size then use it.
# X is specfied by the arg freedom.
# ==================================================================
def get_best_new_edges(bin_size_needed, old_edges, var_bounds, delta=0.1):
    # Make a list of acceptable bin-sizes
    bin_sizes_range = np.linspace(bin_size_needed, (1+delta)*bin_size_needed, 100)
    # Prepare a bool to flag if a binsize was found where
    # all new bin-edges are close to old-edges
    bin_size_found = False
    # Prepare array to hold new edges
    chosen_new_edges = old_edges

    # Loop over binsizes
    for bin_size in bin_sizes_range:
        # Get the number of bin edges corressponding to the bin-size
        num_var_edges_new = int((var_bounds[1] - var_bounds[0]) // bin_size) + 1
        # Get the new edges
        new_edges = np.linspace(*var_bounds, num_var_edges_new)

        # Array to hold bools checking if each candidate
        # new edge is close enough to any old edge
        new_edge_near_an_old_edge = []
        # Loop over candidate new-edges
        for new_edge in new_edges:
            # Subtract away new edge from all old-edges
            old_minus_new = np.absolute(np.array(old_edges)-new_edge)
            # Check if smallest difference is < 0.01
            if(old_minus_new.min() < 0.01):
                # edge is definitly near-enough an old edge
                new_edge_near_an_old_edge.append(True)
            else:
                new_edge_near_an_old_edge.append(False)
        # Check if all new edges were found close to old-edges
        if(not all(near_edge for near_edge in new_edge_near_an_old_edge)):
            continue
        else:
            # If all new-edges were nere old-edges, we're done!
            return new_edges

    # If we come here, no new-edgees were found. Break code
    assert False

    return chosen_new_edges


# ==================================================================
# Function responsible to process the data saved from input file
# and run a masking process in the 2D discrim-var plane for
# bins with efficiency < WP requested
# and then call a drawing function to plot and save the rocs
# ==================================================================
def process_eff_plots(pt_range_to_discrims_to_proc_to_histos_map, pt_range_to_discrims_to_edges_map,
                      var_to_plot, WPs, discrims_asked, outdir, signal='higgs', **plot_args):
    # ================================
    # Masking signal 2D bins with SigEff < WP
    # and getting efficiency 2D histograms
    # ================================

    for pt_range_name, discrim_to_proc_to_histo_map in pt_range_to_discrims_to_proc_to_histos_map.items():
        # Declare a map from discriminant to efficiency histogram with a given Mask
        wp_discrim_to_proc_to_eff_data_dict = {}
        for discrim_name, proc_name_to_histo_map in discrim_to_proc_to_histo_map.items():
            if discrim_name not in discrims_asked:
                continue
            var_edges = pt_range_to_discrims_to_edges_map[pt_range_name][discrim_name][0]
            discrim_edges = pt_range_to_discrims_to_edges_map[pt_range_name][discrim_name][1]

            # Get the masking using signal process to be applied to backgrounds
            for sig_eff in WPs:
                sig_eff = sig_eff/100
                # Get the bools array which tells us which discriminant bins to keep to achieve WP
                discrim_bins_to_keep = get_discrim_bins_to_keep(proc_name_to_histo_map[signal][0],
                                                                discrim_edges, sig_eff)
                # Initalise a dictionary for each signal efficiency requested
                wp_discrim_to_proc_to_eff_data_dict[(str(sig_eff), discrim_name)] = {}
                # Loop over processes histograms
                for proc_name, (hist_wt, hist_wt2) in proc_name_to_histo_map.items():
                    # Get the 2D Histo of efficiencies, the corresponding error histo
                    (eff_hist, eff_error,
                        tagged_hist, total_hist,
                        tagged_err_sq, total_err_sq) = get_efficiency(hist_wt, hist_wt2,
                                                                      discrim_bins_to_keep)
                    # Build a dictionary with all info for the eff vs variable plot!
                    wp_discrim_to_proc_to_eff_data_dict[(str(sig_eff), discrim_name)][proc_name] = \
                        dict(eff_hist=eff_hist, wp=sig_eff,
                             proc=proc_name,
                             var_edges=var_edges, discrim=discrim_name,
                             eff_error=eff_error,
                             tagged_hist=tagged_hist,
                             total_hist=total_hist,
                             tagged_hist_err_sq=tagged_err_sq,
                             total_hist_err_sq=total_err_sq)

        # ================================
        # We need to get the info we have in a format
        # allowing to overlay different discrims
        # per WP per process
        # ================================
        # Loop over eff requested
        for sig_eff in WPs:
            sig_eff = sig_eff/100
            # Get all the process->eff_data maps for this signal eff (from all discriminants)
            proc_to_eff_data_dict_list = [value for keys, value
                                          in wp_discrim_to_proc_to_eff_data_dict.items()
                                          if keys[0] == str(sig_eff)]

            # Map process to a list of efficiency data for all discriminants
            proc_to_discrims_eff_map = defaultdict(list)
            # Loop over the process-> eff_data for each discriminant for this sig eff
            for proc_to_eff_data_dict in proc_to_eff_data_dict_list:
                # Loop over the processes
                for proc_name, eff_data_dict in proc_to_eff_data_dict.items():
                    # append eff_data for this process from each discrim
                    proc_to_discrims_eff_map[proc_name].append(eff_data_dict)
            # Now loop over the processes, and pass the eff data for all discrims to plot
            for proc_name, all_discrims_eff_data in proc_to_discrims_eff_map.items():
                draw_eff_plot(all_discrims_eff_data, outdir, proc_name, var_to_plot,
                              sig_eff_cut=sig_eff, signal=signal, **plot_args)

        # ================================
        # We need to get the info we have in a format
        # allowing to overlay different working points
        # per discrim per process
        # ================================
        for discrim_name in discrims_asked:
            # Get all the process->eff_data maps for this discrim (from all WPs)
            proc_to_eff_data_dict_list = [value for keys, value
                                          in wp_discrim_to_proc_to_eff_data_dict.items()
                                          if keys[1] == discrim_name]
            # Map process to a list of efficiency data for all WPs per discriminant
            proc_to_wps_eff_map = defaultdict(list)
            # Loop over the process-> eff_data for each discriminant for this sig eff
            for proc_to_eff_data_dict in proc_to_eff_data_dict_list:
                # Loop over the processes
                for proc_name, eff_data_dict in proc_to_eff_data_dict.items():
                    # append eff_data for this process from each discrim
                    proc_to_wps_eff_map[proc_name].append(eff_data_dict)

            # Now loop over the processes, and pass the eff data for all WPs to plot
            for proc_name, all_wps_eff_data in proc_to_wps_eff_map.items():
                draw_eff_plot(all_wps_eff_data, outdir, proc_name, var_to_plot, overlay_wps=True,
                              signal=signal, **plot_args)

        # ================================
        # Make variable histos
        # overlaying a bkg with higgs tagged vs total (total 4)
        # We need to get the info we have in a format
        # allowing to overlay different processes
        # per discrim per WP per process
        # ================================

        for discrim_name in discrims_asked:
            for sig_eff in WPs:
                sig_eff = sig_eff/100
                # Get all the process->eff_data maps for this discrim-WP (from all flavs)
                proc_to_eff_data_dict_list = [value for keys, value
                                              in wp_discrim_to_proc_to_eff_data_dict.items()
                                              if keys[1] == discrim_name
                                              and keys[0] == str(sig_eff)]

                proc_to_eff_data_map = defaultdict(list)
                # Now loop over the processes, and pass the eff data for all WPs to plot
                for proc_to_eff_data_dict in proc_to_eff_data_dict_list:
                    bkg_names = []
                    for proc_name, eff_data in proc_to_eff_data_dict.items():
                        proc_to_eff_data_map[proc_name].append(eff_data)
                        if(proc_name != signal):
                            bkg_names.append(proc_name)

                signal_data = proc_to_eff_data_map[signal]
                for bkg in bkg_names:
                    all_eff_data = [signal_data[0], proc_to_eff_data_map[bkg][0]]
                    draw_tag_v_untag_plot(all_eff_data, outdir, bkg, var_to_plot,
                                          sig_eff_cut=sig_eff, **plot_args)


# ==================================================================
# Function returns an arr of bools for each discriminant bin
# telling us which discim bins should be kept to achieve a WP
# ==================================================================
def get_discrim_bins_to_keep(sig_hist, discrim_edges, wp_eff):
    # Get the discrim histogram from the 2D discrim-var histo
    sig_discrim_hist = sig_hist.sum(axis=1)
    sig_eff = get_sigEff(sig_discrim_hist)
    keep_discrim_bin = sig_eff < wp_eff
    # Return the mask
    return keep_discrim_bin


# ==================================================================
# Function to return sig eff from a discriminant histogram
# ==================================================================
def get_sigEff(sig_discrim_hist):
    # ==============
    # Signal eff:
    # =============
    # Get the cumilative sum of truth-matched starting from every bin edges (reverse cumsum)
    sig_eff_num = np.cumsum(sig_discrim_hist[::-1])[::-1]  # exclude overflow and underflow
    # Get the total number of truth-matched jets -- include over/under flow to underestimate eff
    sig_eff_denom = (np.cumsum(sig_discrim_hist[::-1])[::-1]).max()
    # Signal efficiency is ratio
    sig_eff = sig_eff_num/sig_eff_denom
    return sig_eff


# ==================================================================
# Function responsible to calculate efficiency for all processes
# along with error calculation. Takes in the 2D
# discrim-var histo and its cousin with weights^2
# ==================================================================
def get_efficiency(hist_wt, hist_wt2, keep_discrim_bin):

    # Get the variable histogram (excluding under/overflow discrim bins)
    total = hist_wt.sum(axis=0)
    total_err_sq = hist_wt2.sum(axis=0)

    # Get the variable histogram after tagging
    # Prepare tagged 2D histograms as an array of zeroes
    tagged_2d_hist_wt = np.full_like(hist_wt, 0)
    tagged_2d_hist_wt2 = np.full_like(hist_wt2, 0)

    # Only fill entries of the 2D histogram for every variable bin
    # where discriminant values are above threshold for tagging
    tagged_2d_hist_wt[keep_discrim_bin, :] = hist_wt[keep_discrim_bin, :]
    tagged_2d_hist_wt2[keep_discrim_bin, :] = hist_wt2[keep_discrim_bin, :]
    # Get the tagged variable histogram. total discrim bins for each var bin
    # will not contribute (since they are 0 )
    tagged = tagged_2d_hist_wt.sum(axis=0)
    tagged_err_sq = tagged_2d_hist_wt2.sum(axis=0)

    # We define variable bins with no entries to have NaN eff
    eff = np.full_like(total, np.nan)
    eff_error = np.full_like(total, np.nan)
    unempty_bins = total > 0
    # Efficiency
    eff[unempty_bins] = tagged[unempty_bins] / total[unempty_bins]
    # Use binomial error (see line 3026 https://root.cern.ch/doc/master/TH1_8cxx_source.html)
    eff_error = np.sqrt(abs((
                             (1. - 2. * eff) * tagged_err_sq
                             + (eff**2) * (total_err_sq)) / total**2))

    return eff, eff_error, tagged, total, tagged_err_sq, total_err_sq


# ==================================================================
# This is responsible for drawing the Canvas for one process
# here overlay either different discrims or different WPs per discrim
# ==================================================================
# hand-optimized efficiency ranges (delete to get auto range)
SAMPLE_RANGES_LOG = {
    ('dijet', 60): (4, 800),
    ('top', 60): (2, 200),
    ('higgs', 60): (1, 5),
}
SAMPLE_RANGES = {
    #('higgs', 60): (0.4, 0.7),
    ('dijet', 50): (1, 320),
    ('top',   50): (1, 200),
    ('dijet', 60): (1, 220),
    ('top',   60): (1, 75),
    ('dijet', 70): (1, 140),
    ('top',   70): (1, 35),
}


def draw_eff_plot(all_overlaid_eff_data, outdir, proc_name,
                  var_to_plot, sig_eff_cut=None, overlay_wps=False,
                  signal='higgs', log_eff_axis=False, errorbar=False, approved=False):

    # ================================
    # Prepare output file
    # ================================
    outfile = f'{proc_name}_LogScale.pdf' if log_eff_axis else f'{proc_name}.pdf'
    eff_in_percent = 0
    if(not overlay_wps):
        eff_in_percent = float(sig_eff_cut * 100)
        eff_name = f'WP_{eff_in_percent:.0f}'
        outpath = outdir/'overlay_discrims'/eff_name
    else:
        discrim = all_overlaid_eff_data[0]['discrim']
        outpath = outdir/'overlay_wps'/discrim
    outpath.mkdir(exist_ok=True, parents=True)
    # Open a Canvas
    with Canvas(outpath/outfile) as can:
        # Loop over the efficiency info for the different lines to be overlaid
        for eff_data in all_overlaid_eff_data:
            # ================================
            # Prepare input data
            # ================================
            # Name of the discriminant
            discrim_name = eff_data['discrim']
            # The working point selected
            wp = eff_data['wp']
            # The efficiency vs variable histo
            eff_hist = eff_data['eff_hist']
            # The variable axis edges
            var_edges = eff_data['var_edges']
            # Get units to TeV[GeV] if pT[Mass] is plotted
            if(var_to_plot == 'fatjet_pt'):
                var_edges = var_edges * 1e-6
            if(var_to_plot == 'fatjet_mass'):
                var_edges = var_edges * 1e-3
            # Get the variable axis centers
            var_centers = (var_edges[:-1] + var_edges[1:]) / 2
            eff_hist = eff_hist[1:-1]
            # The error on the variable (= bin_width/2)
            var_err = np.stack([var_centers - var_edges[:-1], var_edges[1:] - var_centers])
            # The error on the efficiency
            eff_error = eff_data['eff_error'][1:-1]
            # ================================
            # Prepare some line styling
            # ================================

            # Get an error plotting function ready
            plot = draw_errorbar if errorbar else draw_fill_between
            # Make a plot with errors
            plot(can.ax, proc_name=proc_name, signal=signal,
                 var_centers=var_centers, eff_hist=eff_hist, eff_error=eff_error,
                 discrim=discrim_name, var_err=var_err, wp=wp, overlay_wps=overlay_wps)
        # ================================
        # Prepare some canvas styling
        # ================================
        # X and Y labels, ticks and scales
        # ================
        # Y-AXIS
        # ========
        # Axis title
        process_name = PROCESS_NAME_MAP[proc_name]
        if(proc_name == signal):
            y_label = f'{process_name} Efficiency'
        else:
            y_label = f'{process_name} Rejection'
        can.ax.set_ylabel(y_label, **ylabdic(28))
        # Axis ticks
        can.ax.yaxis.set_major_locator(MaxNLocator(prune='upper'))
        # Scale
        if log_eff_axis:
            log_style(can.ax)
        # Range
        hard_coded_eff_range = SAMPLE_RANGES_LOG if log_eff_axis else SAMPLE_RANGES
        if(not overlay_wps):
            if (proc_name, int(eff_in_percent)) in hard_coded_eff_range:
                can.ax.set_ylim(*SAMPLE_RANGES[proc_name, eff_in_percent])
        # ================
        # X-AXIS
        # =======
        # Axis title
        var_name = DISCRIMINANT_NAME_MAP[var_to_plot]
        can.ax.set_xlabel(fr'Large-$R$ Jet {var_name}', **xlabdic(28))
        # Axis limits
        # The X limits will depend on presence of error bar extending left/right
        lower_bound = var_edges[0] if errorbar else var_centers[0]
        upper_bound = (var_edges[1:] if errorbar else var_centers).max()
        can.ax.set_xlim(lower_bound, upper_bound)
        # ================
        # Adding Text info
        # ================
        # Legends
        can.ax.legend(loc='upper right', frameon=False, fontsize=16)
        add_atlas_label(can.ax, vshift=0.05, internal=(not approved))

        # If we are plotting eff v mass, change mass cut text
        mass_in_presel = True
        if(var_to_plot == 'fatjet_mass'):
            mass_in_presel = False

        if(not overlay_wps):
            add_kinematic_acceptance(can.ax, eff=eff_in_percent,
                                     offset=0.05, mass_in_presel=mass_in_presel)
        else:
            add_kinematic_acceptance(can.ax, offset=0.05, mass_in_presel=mass_in_presel)


# ==================================================================
# This is responsible for making var vs eff plot with shaded region instead of error bar
# ==================================================================
def draw_fill_between(ax, proc_name, signal, var_centers, eff_hist, eff_error,
                      discrim, wp, overlay_wps, **ignore):

    # ================
    # Line Style
    # ================
    discrim_name = DISCRIMINANT_NAME_MAP[discrim]
    discrim_color = DISCRIMINANT_COLOR_MAP[discrim]
    discrim_linestyle = DISCRIMINANT_LS_MAP[discrim]
    # in case we are overlaying WPs
    wp_color = WP_COLOR_MAP[str(wp)]
    # Line label
    label = f'{discrim_name} at {wp*100:.0f}%' if overlay_wps else discrim_name
    # Line color
    line_color = wp_color if overlay_wps else discrim_color
    # ================
    # Eff or Rej plots?
    # ================
    # The y-axis is eff for signal, 1/eff for bkg samples
    if(proc_name == signal):
        yaxis = eff_hist
        errorbars = eff_error
    else:
        yaxis = 1/eff_hist
        rej_err = abs(eff_error/eff_hist**2)
        errorbars = rej_err
    # ================
    # Make the plots
    # ================
    ax.plot(var_centers, yaxis,
            label=label, color=line_color, linestyle=discrim_linestyle,
            linewidth=3)
    # Fill area around line corresponding to error
    ax.fill_between(var_centers,
                    (yaxis-errorbars),
                    (yaxis+errorbars),
                    color=line_color, alpha=0.1)


# ==================================================================
# This is responsible for making var vs eff plot with x and y error bars
# ==================================================================
def draw_errorbar(ax, proc_name, signal, var_centers, eff_hist, eff_error,
                  discrim, wp, overlay_wps, wp_color, var_err):
    # ================
    # Line Style
    # ================
    discrim_name = DISCRIMINANT_NAME_MAP[discrim]
    discrim_color = DISCRIMINANT_COLOR_MAP[discrim]
    discrim_linestyle = DISCRIMINANT_LS_MAP[discrim]
    # in case we are overlaying WPs
    wp_color = WP_COLOR_MAP[str(wp)]
    # Line label
    label = f'{discrim_name} at {wp*100:.0f}% WP' if overlay_wps else discrim_name
    # Line color
    line_color = wp_color if overlay_wps else discrim_color
    # ================================
    # Marker Style
    # ================================
    light_color = to_rgba(line_color, 0.6)
    bar_color = line_color
    markerfacecolor = 'none'
    markeredgecolor = line_color
    markeredgewidth = 2 if discrim_linestyle == '--' else 1
    linewidth = 2 if discrim_linestyle == '--' else 4
    # ================
    # Eff or Rej plots?
    # ================
    # The y-axis is eff for signal, 1/eff for bkg samples
    if(proc_name == signal):
        yaxis = eff_hist
        errorbars = eff_error
    else:
        yaxis = 1/eff_hist
        rej_err = abs(eff_error/eff_hist**2)
        errorbars = rej_err
    # ================
    # Make the plots
    # ================
    ax.errorbar(var_centers, yaxis, errorbars, var_err,
                marker=marker, markersize=10,
                label=label, color=bar_color,
                linestyle='none', linewidth=linewidth,
                markerfacecolor=markerfacecolor,
                markeredgecolor=markeredgecolor,
                markeredgewidth=markeredgewidth)


# ==================================================================
# This is responsible for overlaying the kinemaitc distribution of jets
# passing a WP cut and distribution of all other jets for each process.
# For backgrounds, the Higgs distribution is also overlaid.
# ==================================================================
# Map of process and jet status to color
PROC_TAG_COLOR_MAP = {
    ('dijet', 'tagged'):    'darkred',
    ('dijet', 'total'):    'lightcoral',
    ('top', 'tagged'):    'darkgreen',
    ('top', 'total'):    'limegreen',
    ('higgs', 'tagged'):  'darkblue',
    ('higgs', 'total'):    'deepskyblue',
}


# Draw total/tagged distrubution overlaid
def draw_tag_v_untag_plot(all_overlaid_eff_data, outdir,
                          proc_name, var_to_plot,
                          sig_eff_cut=None,
                          approved=False):

    # ================================
    # Prepare output file
    # ================================
    outfile = f'{proc_name}.pdf'
    eff_in_percent = 0
    eff_in_percent = float(sig_eff_cut * 100)
    eff_name = f'WP_{eff_in_percent:.0f}'
    discrim = all_overlaid_eff_data[0]['discrim']
    outpath = outdir/'tagged_vs_total'/discrim/eff_name
    outpath.mkdir(exist_ok=True, parents=True)

    # Open a Canvas
    with CanvasWithRatio(outpath/outfile) as can:

        # Loop over the efficiency info for the different lines to be overlaid
        for eff_data in all_overlaid_eff_data:
            # ================================
            # Prepare input data
            # ================================
            # Name of the process plotted - note Higgs process is overlaid on all plots
            proc_name = eff_data['proc']
            # Name of the discriminant being for which the eff is calculated
            discrim_name = eff_data['discrim']
            # The working point selected
            wp = eff_data['wp']
            # The variable edges
            var_edges = eff_data['var_edges']
            # Get  correct variable units
            if(var_to_plot == 'fatjet_pt'):
                var_edges = var_edges * 1e-6  # TeV
            if(var_to_plot == 'fatjet_mass'):
                var_edges = var_edges * 1e-3  # GeV
            # Get the variable axis centers
            var_centers = (var_edges[:-1] + var_edges[1:]) / 2
            # The error on the variable (= bin_width/2)
            var_err = np.stack([var_centers - var_edges[:-1], var_edges[1:] - var_centers])
            # Tagged and total Histos
            tagged_hist = eff_data['tagged_hist'][1:-1]
            norm_tagged = tagged_hist / tagged_hist.sum()  # Normalised
            total_hist = eff_data['total_hist'][1:-1]
            norm_total = total_hist / total_hist.sum()  # Normalised
            # Tagged and total Histos Errors squared
            tagged_hist_err_sq = eff_data['tagged_hist_err_sq'][1:-1]
            norm_tagged_err_sq = tagged_hist_err_sq / tagged_hist.sum()**2  # Normalised
            total_hist_err_sq = eff_data['total_hist_err_sq'][1:-1]
            norm_total_err_sq = total_hist_err_sq / total_hist.sum()**2  # Normalised
            # Ratio of Tagged/Total
            ratio = norm_tagged/norm_tagged
            ratio_err = np.sqrt(abs(((1. - 2. * ratio) * norm_tagged_err_sq
                                + (ratio**2) * (norm_total_err_sq)) / norm_total**2))
            # ================================
            # Prepare some line styling
            # ================================
            process_name = PROCESS_NAME_MAP[proc_name]

            # Tagged jets style
            legend_tagged = f'{process_name} tagged @ {eff_in_percent}% WP'
            color_tagged = PROC_TAG_COLOR_MAP[(proc_name, 'tagged')]
            # All jets style
            legend_total = f'{process_name} all jets'
            color_total = PROC_TAG_COLOR_MAP[(proc_name, 'total')]
            # ================================
            # Plotting the distirbutions
            # ================================
            can.ax.plot(var_centers, norm_tagged, label=legend_tagged, color=color_tagged, linewidth=3)
            can.ax.plot(var_centers, norm_total, label=legend_total, color=color_total, linewidth=3)
            # Plot the ratio of tagged/total
            can.ax2.plot(var_centers, norm_tagged/norm_total,
                         color=color_total,
                         linewidth=3)
            # Draw errors as filling on ratio panel
            can.ax2.fill_between(var_centers,
                                 (norm_tagged/norm_total-ratio_err),
                                 (norm_tagged/norm_total+ratio_err),
                                 color=color_total, alpha=0.1)
        # ================================
        # Prepare some canvas styling
        # ================================
        # X and Y labels, ticks and scales
        # ================
        y_label = 'Jets (Normalised)'
        can.ax.set_ylabel(y_label, **ylabdic(28))
        var_name = DISCRIMINANT_NAME_MAP[var_to_plot]
        can.ax2.set_xlabel(fr'Large-$R$ Jet {var_name}', **xlabdic(28))
        can.ax.yaxis.set_major_locator(MaxNLocator(prune='upper'))
        # Ratio panel y-axis range and label 
        set_ratio_style(can.ax2, yrange=(0.45, 1.75))
        # ================
        # X bounds
        # ================
        # The X limits will depend on presence of error bar extending left/right
        lower_bound = 50 if var_to_plot == 'fatjet_mass' else var_centers[0]
        upper_bound = 300 if var_to_plot == 'fatjet_mass' else var_centers.max()
        can.ax2.set_xlim(lower_bound, upper_bound)
        # ================
        # Adding Text info
        # ================
        # If we are plotting mass distribution change mass pre-selection
        mass_in_presel = True
        if(var_to_plot == 'fatjet_mass'):
            mass_in_presel = False
        can.ax.legend(loc='upper right', frameon=False, fontsize=24)
        add_kinematic_acceptance(can.ax, eff=eff_in_percent, offset=0.05,
                                 mass_in_presel=mass_in_presel)
        add_atlas_label(can.ax, vshift=0.05, internal=(not approved))


def set_ratio_style(ax, yrange=None):
    ax.set_ylabel(f'Tagged / Total', fontsize=20)
    if yrange is not None:
        ax.set_ylim(yrange)


if __name__ == '__main__':
    run()
